<?php
include "aggregator_sidebar.php";

?>




    <?php if(isset($_SESSION['index'])) {
        include "dynamic_pages/index.php";

    }?>




<?php if(isset($_SESSION['transactions'])) {
    include "dynamic_pages/transactions.php";

}?>


<?php if(isset($_SESSION['individual_flags'])) {
    include "dynamic_pages/individual_flags.php";

}?>


<?php if(isset($_SESSION['groups'])) {
    include "dynamic_pages/groups.php";

}?>


<?php if(isset($_SESSION['group_flags'])) {
    include "dynamic_pages/group_flags.php";

}?>


<?php if(isset($_SESSION['group_details_page'])) {
    include "dynamic_pages/group_details_page.php";

}?>

<?php if(isset($_SESSION['analytics'])) {
    include "dynamic_pages/analytics.php";

}?>












<?php
include "aggregator_footer.php";

?>

<link rel="stylesheet" type="text/css" href="assets/css/jquery.datetimepicker.css?ver<?php echo $nowDateTime?>">

<script type="text/javascript" language="javascript" src="assets/js/jquery.js?ver<?php echo $nowDateTime?>"></script>

    <script src="assets/js/jquery.datetimepicker.full.js"></script>
<script>
    //fire up Jquery DateTime Picker

    $("#start_date").datetimepicker({
        lang:'ch',
        timepicker:false,
        format:'Y-m-d',
        formatDate:'Y-m-d'
       // minDate:'-2016' // yesterday is minimum date

    });

    $("#end_date").datetimepicker({
        lang:'ch',
        timepicker:false,
        format:'Y-m-d',
        formatDate:'Y-m-d'
       // minDate:'-2016' // yesterday is minimum date

    });
</script>