<?php
session_start();

$nowDateTime = new DateTime('NOW');
$nowDateTime = $nowDateTime->format('Y-m-d H:i:s');

include 'businesslogic.php';

$obj = new businesslogic();
$obj->router();
$allIMTOs=$obj->returnAllIMTOs();
$transactionCount= number_format($obj->returnTransactionCount());
$transactionSum = $obj->returnSumOfTransactions();
$flaggedTransactions= number_format($obj->returnFlaggedTransactions());
$CountOfIMTOs = number_format($obj->returnCountOfIMTOs());
$recentFlaggedTransactions = $obj->returnRecentFlaggedTransactions();
$AllFLaggedCount = $obj->returnAllFLaggedCount();
$RecentTransactions = $obj->returnRecentTransactions();
$RecentTransactionsCount = $obj->returnRecentTransactionsCount();

$RecentGroupFlaggedTransactions = $obj->returnGroupRecentFlaggedTransactions();

$groupDetails = $obj->returnGroupDetails();

$allGroupFlagCount = $obj->returnAllGroupFlagCount();








$repoObj = new Repo();
$db = $repoObj->getConnection();

?>


<!DOCTYPE html>
<html>

<!-- Mirrored from tui2tone.github.io/flat-admin-bootstrap-templates/html/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 12 May 2017 14:09:27 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <title>IMTO - Aggregation Platform</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="assets/css/vendor.css?ver<?php echo $nowDateTime?>">
    <link rel="stylesheet" type="text/css" href="assets/css/flat-admin.css?ver<?php echo $nowDateTime?>">

    <link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css?ver<?php echo $nowDateTime?>">


    <!-- Theme -->
    <link rel="stylesheet" type="text/css" href="assets/css/theme/blue-sky.css">
    <link rel="stylesheet" type="text/css" href="assets/css/theme/blue.css">
    <link rel="stylesheet" type="text/css" href="assets/css/theme/red.css">
    <link rel="stylesheet" type="text/css" href="assets/css/theme/yellow.css">
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css">

    <script src="assets/js/sweetalert.js"></script>

    <script type="text/javascript" language="javascript" src="assets/js/jquery.js?ver<?php echo $nowDateTime?>"></script>
    <script type="text/javascript" language="javascript" src="assets/js/jquery.dataTables.js?ver<?php echo $nowDateTime?>"></script>

    <script type="text/javascript" src="assets/js/vendor.js"></script>
    <script type="text/javascript" src="assets/js/app.js"></script>

</head>
<body>
<div class="app app-default">




<aside class="app-sidebar" id="sidebar">
    <div class="sidebar-header">
        <p align="center" style="margin-top: 25%">
            <img src="assets/images/cbnlogotrans.gif" width="65%">
            <a class="sidebar-brand" href="#" style="height: 39px;color:#536837;"><b>IMT AGREGATOR</b></a>
        </p>

        <button type="button" class="sidebar-toggle">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <div class="sidebar-menu">
        <ul class="sidebar-nav">
            <li class="active">
                <a href="backend.php?cat=index">
                    <div class="icon">
                        <i class="fa fa-tasks" aria-hidden="true"></i>
                    </div>
                    <div class="title">Dashboard</div>
                </a>
            </li>

            <li class="@@menu.messaging">
                <a href="backend.php?cat=transactions">
                    <div class="icon">
                        <i class="fa fa-area-chart" aria-hidden="true"></i>
                    </div>
                    <div class="title">Transactions</div>
                </a>
            </li>
            <li class="dropdown ">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <div class="icon">
                        <i class="fa fa-cube" aria-hidden="true"></i>
                    </div>
                    <div class="title">Flag Management</div>
                </a>
                <div class="dropdown-menu">
                    <ul>
                        <li class="line"></li>
                        <li class="section"><i class="fa fa-file-o" aria-hidden="true"></i> Individual</li>
                       <!-- <li><a href="#">Flag Criteria</a></li>-->
                        <li><a href="backend.php?cat=individual_flags">Individual Flags</a></li>

                        <li class="section"><i class="fa fa-file-o" aria-hidden="true"></i>Group</li>
                       <!-- <li><a href="#">Flag Criteria</a></li>-->
                        <li><a href="#">Create Group</a></li>
                        <li><a href="backend.php?cat=groups">Groups</a></li>
                        <li><a href="backend.php?cat=group_flags">Group Flags</a></li>



                    </ul>
                </div>
            </li>

            <li class="@@menu.messaging">
                <a href="backend.php?cat=analytics">
                    <div class="icon">
                        <i class="fa fa-area-chart" aria-hidden="true"></i>
                    </div>
                    <div class="title">Analytics</div>
                </a>
            </li>

        </ul>
    </div>
</aside>

<script type="text/ng-template" id="sidebar-dropdown.tpl.html">
    <div class="dropdown-background">
        <div class="bg"></div>
    </div>
    <div class="dropdown-container">
        {{list}}
    </div>
</script>


<div class="app-container">

    <nav class="navbar navbar-default" id="navbar">
        <div class="container-fluid">
            <div class="navbar-collapse collapse in">
                <ul class="nav navbar-nav navbar-mobile">
                    <li>
                        <button type="button" class="sidebar-toggle">
                            <i class="fa fa-bars"></i>
                        </button>
                    </li>
                    <li class="logo">
                        <a class="navbar-brand" href="#"><span class="highlight">IMTO</span> AGGREGATOR</a>
                    </li>
                    <li>
                        <button type="button" class="navbar-toggle">
                            <img class="profile-img" src="assets/images/profile.png">
                        </button>
                    </li>
                </ul>

               <!-- <ul class="nav navbar-nav navbar-left">
                    <li class="navbar-title">Dashboard</li>
                    <li class="navbar-search hidden-sm">
                        <input id="search" type="text" placeholder="Search..">
                        <button class="btn-search"><i class="fa fa-search"></i></button>
                    </li>
                </ul>-->

                <ul class="nav navbar-nav navbar-right">


                    </li>

                    <li class="dropdown notification warning">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <div class="icon"><i class="fa fa-money" aria-hidden="true"></i></div>
                            <div class="title">Recent Transaction Notifications</div>
                            <div class="count"><?php echo count($RecentTransactionsCount) ?></div>
                        </a>
                        <div class="dropdown-menu">
                            <ul>
                                <li class="dropdown-header"> <?php echo number_format(count($RecentTransactionsCount)) ?> Recent Transactions</li>

                                <?php foreach ($RecentTransactions  as $value => $row) :  ?>

                                <li>
                                    <a href="#">
                                        <span class="badge badge-warning pull-right">--</span>
                                        <div class="message">
                                            <img class="profile" src="https://placehold.it/100x100">
                                            <div class="content">
                                                <div class="title"><?php echo $row['beneficiary_name'] ?> Recieved  - <?php echo  $row['originating_currency_code'].' '.  number_format($row['originating_amount'], 2, '.', ',') ?> </div>
                                                <div class="description">via <?php echo $row['name'].' - ('.$row['code'].')'. ' On <br>'. $obj->returnYYYmmDDformattedDate($row['transaction_date']) ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <?php endforeach; ?>






                                <li class="dropdown-footer">
                                    <a href="#">View All <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </li>


                    <li class="dropdown notification danger">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <div class="icon"><i class="fa fa-bell" aria-hidden="true"></i></div>
                            <div class="title">Transaction Flags</div>
                            <div class="count"><?php echo count($AllFLaggedCount); ?></div>
                        </a>

                        <div class="dropdown-menu">


                            <ul>
                                <li class="dropdown-header"><strong><?php echo count($AllFLaggedCount); ?></strong> New Flags </li>



                                <?php foreach ($recentFlaggedTransactions  as $value => $row) :  ?>

                                <li>
                                    <a href="#">
                                        <span class="badge badge-danger pull-right"><i class="fa fa-flag" aria-hidden="true"></i></span>
                                        <div class="message">
                                            <div class="content">
                                                <div class="title"> <?php echo $row['beneficiary_name'] ?> - <?php echo $row['comment'] ?> </div>
                                                <div class="description">via <?php echo $row['name'].' - ('.$row['code'].')'. ' On <br>'. $obj->returnYYYmmDDformattedDate($row['transaction_date']) ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <?php endforeach; ?>




                                <li class="dropdown-footer">
                                    <a href="#">View All <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>

                        </div>
                    </li>


                    <li class="dropdown notification danger">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <div class="icon"><i class="fa fa-power-off" aria-hidden="true"></i></div>
                            <div class="title">Log Out</div>
                        </a>

                        <div class="dropdown-menu">
                            <ul>
                                <li class="dropdown-header">Log Out</li>

                            </ul>
                        </div>
                    </li>







                </ul>
            </div>
        </div>
    </nav>
