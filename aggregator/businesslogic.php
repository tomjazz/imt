<?php
//define('__ROOT__', dirname(dirname(__FILE__)));
//require_once(__ROOT__ . '/Repo.php');

include "Repo.php";
class businesslogic{


    public function router()
    {


        if (isset($_GET["cat"]))
        {
            switch ($_GET["cat"])
            {
                case "index":
                    $this->indexRedirect();
                    break;


                case "transactions":
                    $this->getTransactions();
                    break;

                case "individual_flags":
                    $this->getIndividualFlags();
                    break;


                case "groups":
                    $this->getGroups();
                    break;

                case "group_flags":
                    $this->getGroupFlags();
                    break;

                case "group_details_page":
                    $this->getGroupDetailsPage();
                    break;

                case "analytics":
                    $this->loadAnalyticsPage();
                    break;



                default:
            }
        }


        if (isset($_POST['auth'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $this->authentication($username, $password);

        }




    }







    public function authentication($username,$password) {

        $hashedPassword = hash('sha256', $password);
        $repoObj = new Repo();
        $db = $repoObj->getConnection();

        $result = $db->prepare("SELECT * FROM operators WHERE username=:username AND password=:password ");
        $result->execute([
            "username" => $username,
        "password"=> $hashedPassword
       ] );

        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {

                $_SESSION['operator_id'] = $rows["id"];
                $_SESSION['back_end_username'] = $rows["username"];
                $_SESSION['role_id'] = $rows["operator_role_fk"];

            }
        }else{
            $_SESSION['error'] ="invalid username or password";
        }
    }




public function indexRedirect(){

    $_SESSION['index'] ='index';
    unset($_SESSION['transactions']);
    unset($_SESSION['individual_flags']);
    unset($_SESSION['groups']);
    unset($_SESSION['group_flags']);
    unset($_SESSION['group_details_page']);

    unset($_SESSION['analytics']);



}

    public function getTransactions(){

        $_SESSION['transactions'] ='transactions';
        unset($_SESSION['index']);
        unset($_SESSION['individual_flags']);
        unset($_SESSION['groups']);
        unset($_SESSION['group_flags']);
        unset($_SESSION['group_details_page']);
        unset($_SESSION['analytics']);


    }


    public function getIndividualFlags(){

        $_SESSION['individual_flags'] ='individual_flags';
        unset($_SESSION['index']);
        unset($_SESSION['transactions']);
        unset($_SESSION['groups']);
        unset($_SESSION['group_flags']);
        unset($_SESSION['group_details_page']);
        unset($_SESSION['analytics']);

    }



    public function getGroups(){

        $_SESSION['groups'] ='groups';
        unset($_SESSION['index']);
        unset($_SESSION['transactions']);
        unset($_SESSION['individual_flags']);
        unset($_SESSION['group_flags']);
        unset($_SESSION['group_details_page']);
        unset($_SESSION['analytics']);

    }


    public function getGroupFlags() {
        $_SESSION['group_flags'] ='group_flags';
        unset($_SESSION['index']);
        unset($_SESSION['transactions']);
        unset($_SESSION['individual_flags']);
        unset($_SESSION['groups']);
        unset($_SESSION['group_details_page']);
        unset($_SESSION['analytics']);




    }


    public function getGroupDetailsPage() {

      $_SESSION['group_details_page'] ='group_details_page';
        unset($_SESSION['index']);
        unset($_SESSION['transactions']);
        unset($_SESSION['individual_flags']);
        unset($_SESSION['groups']);
        unset($_SESSION['group_flags']);
        unset($_SESSION['analytics']);



    }


    public function loadAnalyticsPage() {

        $_SESSION['analytics'] ='analytics';
        unset($_SESSION['index']);
        unset($_SESSION['transactions']);
        unset($_SESSION['individual_flags']);
        unset($_SESSION['groups']);
        unset($_SESSION['group_flags']);
        unset($_SESSION['group_details_page']);



    }




    public function returnAllIMTOs(){
        $repoObj = new Repo();

        $db = $repoObj->getConnection();

        try {
            $sql = "SELECT * FROM imto";
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result > 0) {


                return $result;

                // return $result;
            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }

    }



    public function returnIMTOwithID($id){
        $repoObj = new Repo();

        $db = $repoObj->getConnection();

        try {
            $sql = "SELECT * FROM imto WHERE id=:id";
            $stmt = $db->prepare($sql);
            $stmt->execute(array(
                ":id" => $id
                   ));
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($result > 0) {


                return  $result['name'].' - ('.$result['code'].')';

                // return $result;
            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }

    }



    public function returnTransactionCount(){
        $repoObj = new Repo();

        $db = $repoObj->getConnection();

        try {
            $sql = "SELECT * FROM imto_transactions";
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result > 0) {


                return count($result);

            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }

    }


    public function returnSumOfTransactions(){
        $repoObj = new Repo();

        $db = $repoObj->getConnection();

        $query = $db->prepare("SELECT SUM(recieving_amount) FROM imto_transactions" );

        try{ $query->execute();

            $total = $query->fetch(PDO::FETCH_NUM);
            $summ = $total[0]; // 0 is the first array. here array is only one.
            return $summ;

        } catch(PDOException $e){
            die($e->getMessage());
        }

    }



    public function returnFlaggedTransactions(){
        $repoObj = new Repo();

        $db = $repoObj->getConnection();

        try {
            $sql = "SELECT * FROM imto_transactions WHERE isFlagged=:bool";
            $stmt = $db->prepare($sql);
            $stmt->execute([
                'bool' => 1
            ]);

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result > 0) {


                return count($result);

            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }

    }



    public function returnCountOfIMTOs(){
        $repoObj = new Repo();

        $db = $repoObj->getConnection();

        try {
            $sql = "SELECT * FROM imto";
            $stmt = $db->prepare($sql);
            $stmt->execute();


            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result > 0) {
                return count($result);

            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }

    }




    public function returnRecentFlaggedTransactions(){
        $repoObj = new Repo();

        $db = $repoObj->getConnection();

        try {
            $sql = "
SELECT * FROM imto_transactions
INNER JOIN individual_flag_log ON individual_flag_log.imto_transactions_id=imto_transactions.id
INNER JOIN imto ON imto_transactions.imto_id=imto.id
ORDER BY transaction_date DESC
LIMIT 4
";
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result > 0) {


                return $result;

                // return $result;
            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }

    }



    public function returnGroupRecentFlaggedTransactions(){
        $repoObj = new Repo();

        $db = $repoObj->getConnection();

        try {
            $sql = "
SELECT *
FROM group_flag_log gfl
INNER JOIN group_flag gf ON gf.id = gfl.group_flag_id
INNER JOIN imto_transactions it ON it.id =gfl.imto_transactions_id
ORDER BY transaction_date DESC
LIMIT 4
";
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result > 0) {





                return $result;

                // return $result;
            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }

    }




    public function returnGroupDetails(){

        $repoObj = new Repo();

        $db = $repoObj->getConnection();

        try {
            $sql = "SELECT * FROM group_flag ORDER BY id DESC LIMIT 5";

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result > 0) {


                return $result;

                // return $result;
            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }


    }



    public function returnGroupDetailsByID($id){
        $repoObj = new Repo();

        $db = $repoObj->getConnection();
        $sql = "SELECT * FROM group_flag WHERE id=:id";

        $stmt = $db->prepare($sql);
        $stmt->execute([
            "id" => $id
        ]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result > 0) {


             return $result;
        }

    }





    public function returnGroupFlagCount($flag_flag_id)
    {
        $repoObj = new Repo();

        $db = $repoObj->getConnection();
        $sql = "SELECT * FROM group_flag_log WHERE group_flag_id=:group_flag_id";

        $stmt = $db->prepare($sql);
        $stmt->execute([
            "group_flag_id" => $flag_flag_id
        ]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result > 0) {


            return count($result);

            // return $result;
        }


    }



    public function returnAllGroupFlagCount()
    {
        $repoObj = new Repo();

        $db = $repoObj->getConnection();
        $sql = "SELECT * FROM group_flag_log";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result > 0) {

            return count($result);

            // return $result;
        }


    }






    public function returnCountOfGroupMembers($flag_flag_id)
    {
        $repoObj = new Repo();

        $db = $repoObj->getConnection();
        $sql = "SELECT * FROM group_flag_members WHERE group_flag_id=:group_flag_id";

        $stmt = $db->prepare($sql);
        $stmt->execute([
            "group_flag_id" => $flag_flag_id
        ]);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result > 0) {


            return count($result);

            // return $result;
        }


    }






    public function returnAllFLaggedCount(){
        $repoObj = new Repo();

        $db = $repoObj->getConnection();

        try {
            $sql = "
SELECT * FROM imto_transactions
INNER JOIN individual_flag_log ON individual_flag_log.imto_transactions_id=imto_transactions.id
INNER JOIN imto ON imto_transactions.imto_id=imto.id
ORDER BY transaction_date DESC
";
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result > 0) {


                return $result;

                // return $result;
            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }

    }




    public function returnRecentTransactions(){
        $repoObj = new Repo();

        $db = $repoObj->getConnection();

        try {
            $sql = "
SELECT * FROM imto_transactions it
INNER JOIN imto ON imto.id=it.imto_id
WHERE it.transaction_date >= CURDATE() - INTERVAL 30 DAY
ORDER BY it.transaction_date DESC
LIMIT 3
";
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result > 0) {


                return $result;

                // return $result;
            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }

    }




    public function returnRecentTransactionsCount(){
        $repoObj = new Repo();

        $db = $repoObj->getConnection();

        try {
            $sql = "
SELECT * FROM imto_transactions it
INNER JOIN imto ON imto.id=it.imto_id
WHERE it.transaction_date >= CURDATE() - INTERVAL 30 DAY
ORDER BY it.transaction_date DESC
";
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result > 0) {


                return $result;

                // return $result;
            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }

    }



    public function returnYYYmmDDformattedDate($date){
        $formattedDate = new DateTime($date);
        $formattedDate = $formattedDate->format('Y-m-d');
        return $formattedDate;

    }



    public function currencyConverter($convert_from, $convert_to){

            $repoObj = new Repo();

            $db = $repoObj->getConnection();

            try {
                $sql = "SELECT * FROM exchange_rate WHERE convert_from=:convert_from and convert_to=:convert_to";
                $stmt = $db->prepare($sql);
                $stmt->execute(array(
                    ":convert_from" => $convert_from,
                    ":convert_to" => $convert_to

                ));


                $result = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($result > 0) {
                    return $result['resolved_value'];

                } else {
                    return false;
                }
            }catch (PDOException $ex){
                return false;
            }



    }





    public function updateDiamondPricing($imto_id){
        $repoObj = new Repo();
        $db = $repoObj->getConnection();


        // $nowDateTime = new DateTime('NOW');
       // $nowDateTime = $nowDateTime->format('Y-m-d H:i:s');
      //  $imto_id = $_GET['imto_transactions_id'];

        $stmt = $db->prepare("UPDATE imto_transactions SET isFlagged=:bool WHERE id=:id");
        $stmt->bindValue(":bool",0);
        $stmt->bindValue(":id",$imto_id);
        $result =  $stmt->execute();


        if($result){

            echo "<script language='JavaScript'>
                        swal(
                        'correct',
                        'You have successfully Unflagged Transactions',
                        'success'
                        ); </script>";



            // header("Refresh:3; url=backend.php?cat=diamond_pricing");
            // header("Refresh:1");



        }else{
            echo "<script language='JavaScript'>
                        swal(
                        'Oops!!!',
                        'Unable to Update ',
                        'error'
                        ); </script>";
        }

    }








}