<!--
<div class="row">
    <div class="container-fluid">
        <div class="col-lg-12 search-box">
            <h3 align="center" style="margin-top: 1%">View Summary for Period: </h3>
            <form>
                <p align="center">
                    <label>Start Date:</label><input type="text" placeholder="Pick Date" name="" id="start_date">
                    <label style="margin-left: 1%"> End Date: </label>  <input type="text" placeholder="Pick Date" name="" id="end_date">


                </p>
                <p align="center">
                    <button type="submit" value="Search" class="btn btn-large btn-danger " name="">
                        <i class="fa fa-search" aria-hidden="true"></i>  Search by date

                    </button>
                </p>
            </form>
        </div>
    </div>
</div>
-->

<div class="row">


    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <a class="card card-banner card-blue-light">
            <div class="card-body">
                <i class="icon fa fa-bars fa-4x"></i>
                <div class="content">
                    <div class="title">TRANSACTION VOLUME</div>
                    <div class="value"><span class="sign"></span><?php echo $transactionCount ?></div>
                </div>
            </div>
        </a>

    </div>


    <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
        <a class="card card-banner card-blue-light">
            <div class="card-body">
                <i class="icon fa fa-line-chart fa-4x"></i>
                <div class="content">
                    <div class="title">SUCCESSFUL TRANSACTIONS </div>

                    <div class="value"><span class="sign">  </span> &#036; <?php echo number_format( $obj->currencyConverter("NGN","USD")  * $transactionSum , 2, '.', ',')?> </div> <!--&#8358;-->
                </div>
            </div>
        </a>

    </div>


    <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
        <a class="card card-banner card-blue-light">
            <div class="card-body">
                <i class="icon fa fa-flag fa-4x"></i>
                <div class="content">
                    <div class="title">FLAGGED </div>
                    <div class="value"><span class="sign"></span> <?php echo $flaggedTransactions + $allGroupFlagCount?> </div>
                </div>
            </div>
        </a>

    </div>

    <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
        <a class="card card-banner card-blue-light">
            <div class="card-body">
                <i class="icon fa fa-user-plus fa-4x"></i>
                <div class="content">
                    <div class="title">ACTIVE IMTOs </div>
                    <div class="value"><span class="sign"></span><?php echo $CountOfIMTOs?></div>
                </div>
            </div>
        </a>

    </div>
    <!--
      <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
          <a class="card card-banner card-blue-light">
      <div class="card-body">
        <i class="icon fa fa-globe fa-4x"></i>
        <div class="content">
          <div class="title">FLAG COUNTRY </div>
          <div class="value"><span class="sign"></span>25</div>
        </div>
      </div>
    </a>

      </div>
    -->

</div>


<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card card-mini">
            <div class="card-header">
                <div class="card-title">Recently Opened Tracking Groups</div>
                <ul class="card-action">

                </ul>
            </div>
            <div class="card-body no-padding table-responsive">
                <table class="table card-table">
                    <thead>
                    <tr>
                        <th>GROUP</th>
                        <th class="right">MEMBERS</th>
                        <th>COUNT OF FLAGS</th>
                    </tr>
                    </thead>
                    <tbody>



                    <?php foreach ($groupDetails  as $value => $row) :  ?>

                    <a href="backend.php?cat=groups">
                        <tr>
                            <td>

                            <a href='backend.php?cat=group_details_page&groupID=<?php echo $row['id'] ?>'  class="">   <?php echo $row['group_name'] ?> </a>
                            </td>
                            <td class="right"> <?php print_r($obj->returnCountOfGroupMembers($row['id'])); ?> </td>
                            <td> <a href="backend.php?cat=groups"><span class="badge badge-warning badge-icon" ><i class="fa fa-flag" aria-hidden="true"></i><span> <?php print_r($obj->returnGroupFlagCount($row['id'])); ?></span></span>   </a></td>

                        </tr>

                    <?php endforeach; ?>



                    </tbody>
                </table>

                <div align="right" class="dropdown-footer">
                    <a href="backend.php?cat=groups" class="btn">View All <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>

        <br>

        <div class="card card-mini">
            <div class="card-header">
                <div class="card-title"><i class="fa fa-line-chart" aria-hidden="true"></i> &nbsp;  <strong><?php echo count($RecentTransactions) ?> </strong> Recent Transactions </div>
                <ul class="card-action">

                </ul>
            </div>
            <div class="card-body no-padding table-responsive">
                <table class="table card-table">
                    <thead>
                    <tr>
                        <th>IMTO</th>
                        <th>DATE</th>
                        <th>RECIEVING AMOUNT</th>
                    </tr>
                    </thead>
                    <tbody>



                    <?php foreach ($RecentTransactions  as $value => $row) :  ?>
                    <a href="backend.php?cat=groups">
                        <tr>
                            <td>

                                <?php echo $obj->returnIMTOwithID($row['imto_id']) ?>
                            </td>
                            <td lass="tg-i81m"><?php echo $row['transaction_date']; ?></td>

                            <td lass="tg-i81m"><?php echo $row['originating_currency_code'] . ' ' . number_format($row['originating_amount'], 2, '.', ','); ?> </td>

                        </tr>

                        <?php endforeach; ?>



                    </tbody>
                </table>

                <div align="right" class="dropdown-footer">
                    <a href="backend.php?cat=transactions" class="btn">View All <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card card-mini">
            <div class="card-header">
                <div class="card-title">Recently flagged </div>
                <ul class="card-action">

                </ul>
            </div>
            <div class="card-body no-padding table-responsive">
                <table class="table card-table">
                    <thead>
                    <tr>
                        <th><i class="fa fa-user" aria-hidden="true"></i>
                            Beneficiary
                        </th>
                        <th>Flag Narrative</th>

                    </tr>
                    </thead>
                    <tbody>


                    <?php foreach ($RecentGroupFlaggedTransactions  as $value => $row) :  ?>

                        <tr>
                            <td>

                                <?php echo $row['beneficiary_name'] ?>
                            </td>
                            <td><a href="backend.php?cat=group_flags"><span class="badge badge-danger badge-icon" style="padding: 2%;"><i class="fa fa-flag" aria-hidden="true"></i><span><?php echo $row['comment']. ' On <br>'. $obj->returnYYYmmDDformattedDate($row['transaction_date']) ?></span></span></a></td>

                        </tr>

                    <?php endforeach; ?>




                    <?php foreach ($recentFlaggedTransactions  as $value => $row) :  ?>

                        <tr>
                            <td>

                                <?php echo $row['beneficiary_name'] ?>
                            </td>
                            <td><a href="backend.php?cat=individual_flags" ><span class="badge badge-danger badge-icon" style="padding: 2%;"><i class="fa fa-flag" aria-hidden="true"></i><span><?php echo $row['comment']. ' On :'. $obj->returnYYYmmDDformattedDate($row['transaction_date']) ?></span></span></a></td>

                        </tr>

                    <?php endforeach; ?>



                    </tbody>
                </table>


                <div align="right" class="dropdown-footer">
                    <a href="backend.php?cat=individual_flags" class="btn">View All <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>








</div>




