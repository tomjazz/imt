<?php
/**
 * Created by PhpStorm.
 * User: Tomiwa
 * Date: 5/17/2017
 * Time: 8:58 PM
 */

if(isset($_GET['groupID'])) {
    $groupDetailsByID = $obj->returnGroupDetailsByID($_GET['groupID']);

?>






    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="card card-mini">
                <div class="card-header">
                    <div class="card-title"> <h3>Group Details</h3></div>
                    <ul class="card-action">

                    </ul>
                </div>
                <div class="card-body no-padding table-responsive">

                    <div align="center" class="">
                        <a href="backend.php?cat=groups" class="btn" style="color:#fff; background: #808080">Edit Group Details </a>


                    </div>

                    <table class="table card-table">

    <?php foreach ($groupDetailsByID  as $value => $row) :  ?>

                        <tr>
                            <th>NAME:</th>
                            <td><?php echo $row['group_name'] ?> </td>
                        </tr>
                        <tr>
                            <th>IDENTIFIER:</th>
                            <td> <?php echo $row['group_identifier'] ?>  </td>
                        </tr>

                        <tr>
                            <th>DESCRIPTION:</th>
                            <td> <?php echo $row['group_description'] ?>  </td>
                        </tr>

                        <tr>
                            <th>MEMBERS:</th>
                            <td class="right"> <?php print_r($obj->returnCountOfGroupMembers($row['id'])); ?> </td>
                        </tr>

                         <tr>
                            <th> COUNT OF FLAGS:</th>
                            <td> <span class="badge badge-warning badge-icon" style="padding: 3%"><i class="fa fa-flag" aria-hidden="true"></i><span> <?php print_r($obj->returnGroupFlagCount($row['id'])); ?></span></span>  </td>
                        </tr>


                        <tr>
                            <th>COUNTRY:</th>
                            <td> <?php echo $row['country'] ?>  </td>
                        </tr>

                        <tr>
                            <th>STATE:</th>
                            <td> <?php echo $row['state'] ?>  </td>
                        </tr>

                        <tr>
                            <th>ADDRESS:</th>
                            <td> <?php echo $row['address'] ?>  </td>
                        </tr>

        <tr>
            <th>KEYWORDS:</th>
            <td class="right"><?php echo $row['keywords'] ?> </td>
        </tr>



                       </table>


                </div>
            </div>
        </div>






        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="card card-mini">
                <div class="card-header">
                    <div class="card-title"> <h3>Group Memeber Details</h3></div>
                    <ul class="card-action">

                    </ul>
                </div>
                <div class="card-body no-padding table-responsive">

                    <div align="center" class="">
                        <a href="#" class="btn" style="color:#fff; background: #808080">Add New Member </a>


                    </div>

                    <table class="dataTable ">
                        <thead>
                        <tr>
                            <th>NAME</th>
                            <th>BVN</th>
                          <!--  <th>PHONE</th>
                            <th>EMAIL</th>-->
                            <th>ADDRESS</th>
                           <!-- <th>DOB</th> -->

                        </tr>
                        </thead>

                        <?php
                        $sql="SELECT * FROM group_flag_members WHERE group_flag_id=:gfd";
                        $result = $db->prepare($sql);

                        $result->execute([
                            'gfd' => $row['id']
                        ]);




                        //  }
                        if ($result->rowCount()){


                        while ($member = $result->fetch(PDO::FETCH_ASSOC)) {
                        ?>

                        <tbody>
                        <tr>
                            <td lass="tg-i81m"><?php echo $member['name']; ?></td>
                            <td lass="tg-i81m"><?php echo $member['bvn']; ?></td>

                            <td lass="tg-i81m"><?php echo $member['address']; ?></td>


                        </tr>

                        <?php
                        }
                        }else{
                            echo "No Group Members";

                        }

                        ?>

                        <?php endforeach; ?>

                        </tbody>

                    </table>



                </div>
            </div>
        </div>



    </div>







<?php }?>