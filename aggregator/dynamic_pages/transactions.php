



<!-- Begining of Filter Container -->

<div class="row">
    <div class="container earch-box">
        <h3 align="center" style="margin-top: 1%">Filter Transactions: </h3>

        <div class="panel panel-default">
            <div class="panel-body">
                <form class="form-inline" method="post" action="#">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h4 class="muted">DATE RANGE</h4>

                                    <p></p>

                                    <div class="input-daterange input-group" id="datepicker">
                                        <input type="text" class=" form-control" name="start_date"
                                               id="start_date" placeholder="YYYY-MM-DD"/>
                                        <span class="input-group-addon"> <strong>-</strong> </span>
                                        <input type="text" class="form-control" name="end_date"
                                               id="end_date" placeholder="YYYY-MM-DD"/>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h4 class="muted">IMTOs</h4>

                                    <p></p>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <select class="form-control imto" name="imto" id="imto" >
                                                <option value="0">----------------------Select--------------------</option>
                                                <option value="0">All IMTOs</option>

                                                <?php foreach ($allIMTOs  as $value => $row) :  ?>
                                                    <option value="<?php echo $row['id'] ?>"> <?php echo $row['name']." (". $row['code']." )" ?> </option> <?php endforeach; ?>


                                            </select>

                                            <p> &nbsp;</p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>


                        <div class="col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h4 class="muted">BVN</h4>

                                    <p></p>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="text" id="beneficiary_bvn"
                                                   name="beneficiary_bvn"
                                                   placeholder="Input Beneficiary BVN"
                                                   class="form-control"
                                                   style="background:#fff; 47px ;width: 300px;">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div align="center">
                        <button type="submit" id='filterGridResult'
                                name="filterGridResult" class="btn btn-large btn-info"
                                style=" ">
                            <i class="fa fa-search" aria-hidden="true"></i> Search
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>


<!--End of Filter Container -->

<div class="row">
    <p>&nbsp;</p>
    <p>&nbsp;</p>




    <div class="col-xs-12">
        <div class="card">
           <!-- <div class="card-header">
                TRANSACTIONS
            </div>-->

            <div class="card-body no-padding">

                <?php

                $num_rec_per_page=10;

                if (isset($_GET["page"])) {
                    $page  = $_GET["page"];
                } else {
                    $page=1;
                }


                $start_from = ($page-1) * $num_rec_per_page;
                $result ="";
                if(isset($_POST["filterGridResult"])){

                $start_date = new DateTime( $_POST['start_date'] );
                $end_date   = new DateTime( $_POST['end_date'] );
                $imto = $_POST["imto"];
                $bvn = $_POST["beneficiary_bvn"];


                //DATE RANGE Only
                if(isset($start_date) && isset($end_date) && $start_date!=null  && $end_date !=null && $imto==0){
                    $sql = "SELECT * FROM imto_transactions WHERE transaction_date BETWEEN :start_date AND :end_date  ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
                    $result = $db->prepare($sql);
                    $result->execute( array(
                        ":start_date" => $start_date->format( "Y-m-d" ),
                        ":end_date"   => $end_date->format( "Y-m-d" )
                ));




                }

                //DATE RANGE and IMTO Only
                if(isset($start_date) && isset($end_date) && $start_date!=null  && $end_date !=null && $imto !=null && $imto > 0) {
                    $sql = "SELECT * FROM imto_transactions WHERE transaction_date BETWEEN :start_date AND :end_date AND imto_id=:imto  ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
                    $result = $db->prepare($sql);
                    $result->execute(array(
                        ":start_date" => $start_date->format("Y-m-d"),
                        ":end_date" => $end_date->format("Y-m-d"),
                        ":imto" =>$imto

                    ));

                }



                //DATE RANGE and BVN Only
                if(isset($start_date) && isset($end_date)  && isset($bvn) && $start_date!=null  && $end_date !=null &&  $bvn !=null &&  $imto !=null &&  $imto == 0) {

                    $sql = "SELECT * FROM imto_transactions WHERE transaction_date BETWEEN :start_date AND :end_date AND beneficiary_bvn=:beneficiary_bvn  ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
                    $result = $db->prepare($sql);
                    $result->execute(array(
                        ":start_date" => $start_date->format("Y-m-d"),
                        ":end_date" => $end_date->format("Y-m-d"),
                        ":beneficiary_bvn" =>$bvn

                    ));
                }



                //BVN ONLY
                if(isset($bvn) && $bvn!=null ){

                    $sql = "SELECT * FROM imto_transactions WHERE beneficiary_bvn=:beneficiary_bvn  ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
                    $result = $db->prepare($sql);
                    $result->execute(array(
                          ":beneficiary_bvn" =>$bvn

                    ));

                }


                //DATE-RANGE, IMTO, BVN
                if(isset($start_date) && isset($end_date) && isset($bvn) && $start_date!=null  && $end_date !=null && $imto !=null && $imto > 0  && $bvn!=null ){

                    $sql = "SELECT * FROM imto_transactions WHERE transaction_date BETWEEN :start_date AND :end_date AND imto_id=:imto AND beneficiary_bvn=:bvn ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
                    $result = $db->prepare($sql);
                    $result->execute(array(
                        ":start_date" => $start_date->format("Y-m-d"),
                        ":end_date" => $end_date->format("Y-m-d"),
                        ":imto" =>$imto,
                        ":bvn" =>$bvn

                    ));

                }



                if ($result->rowCount()){


                ?>





                <table class="dataTable ">
                    <thead>
                    <tr>
                        <th>IMTO</th>
                        <th>REF</th>
                        <th>DATE</th>
                        <th>INITIATOR</th>
                        <th>PAYMENT CHANNEL</th>

                        <th>TRANSFER AMOUNT</th>
                        <th>RECIEVING AMOUNT</th>

                        <th>BENEFICIARY</th>
                        <th>BENEFICIARY BVN</th>
                        <th>RECIEVING CHANNEL</th>

                    </tr>
                    </thead>

                    <?php
                    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    ?>

                    <tbody>
                    <tr>
                        <td lass="tg-i81m"><?php echo $obj->returnIMTOwithID($row['imto_id']) ?></td>
                        <td lass="tg-i81m"><?php echo $row['transaction_ref']; ?></td>
                        <td lass="tg-i81m"><?php echo $row['transaction_date']; ?></td>
                        <td lass="tg-i81m"><?php echo $row['initiator_name']; ?></td>

                        <td lass="tg-i81m"><?php echo $row['payment_channel']; ?></td>

                        <td lass="tg-i81m"><?php echo $row['originating_currency_code'] . ' ' . number_format($row['originating_amount'], 2, '.', ','); ?> </td>
                        <td lass="tg-i81m"><?php echo $row['destination_currency_code'] . ' ' . number_format($row['recieving_amount'], 2, '.', ','); ?> </td>
                        <td lass="tg-i81m"><?php echo $row['beneficiary_name']; ?></td>
                        <td lass="tg-i81m"><?php echo $row['beneficiary_bvn']; ?></td>
                        <td lass="tg-i81m"><?php echo $row['recieving_channel']; ?></td>

                    </tr>

                    <?php
                    }
                    }else{
                        echo "No transaction found";

                    }

                    }?>
                    </tbody>

                </table>



                <?php

                if(isset($_POST["filterGridResult"])) {

                  /**  $sql = "SELECT * FROM imto_transactions WHERE transaction_ref=:CustomerBookingSearchBox ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
                    $result = $db->prepare($sql);
                    $result->bindValue(":CustomerBookingSearchBox", $_POST["CustomerBookingSearchBox"]);
                    $result->execute();
**/
                //DATE RANGE Only
                if(isset($start_date) && isset($end_date) && $start_date!=null  && $end_date !=null && $imto==0) {
                    $sql = "SELECT * FROM imto_transactions WHERE transaction_date BETWEEN :start_date AND :end_date  ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
                    $result = $db->prepare($sql);
                    $result->execute(array(
                        ":start_date" => $start_date->format("Y-m-d"),
                        ":end_date" => $end_date->format("Y-m-d")
                    ));
                }


                    $rs_result = $result->fetch(PDO::FETCH_ASSOC);
                    $total_records = count($rs_result);  //count number of records
                    $total_pages = ceil($total_records / $num_rec_per_page);
                    if ($rs_result) {


                        echo "<div align=''>
         <a href='backend.php?cat=transactions&page=1'>" . '|<' . "</a> "; // Goto 1st page

                        for ($i = 1; $i <= $total_pages; $i++) {
                            echo "<a class='nav_pagination' href='backend.php?cat=transactions&page=" . $i . "'>" . $i . "</a>";
                        };
                        echo "<a href='backend.php?cat=transactions&page=$total_pages'>" . '>|' . "</a>
        </div>"; // Goto last page
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>









