<?php

if(isset($_GET['imto_transactions_id'])) {
 $obj->updateDiamondPricing($_GET['imto_transactions_id']);

    echo $_GET['imto_transactions_id'];
}

?>





<!--
<div class="row">
    <div class="container-fluid">

        <div class="col-lg-12 search-box">
            <h3 align="center" style="margin-top: 1%">Filter Individual FLagged Transactions: </h3> <br>
            <form method="post">
                <p align="center">
                    <label>Start Date:</label><input type="text" placeholder="Pick Date" name="start_date" id="start_date">
                    <label style="margin-left: 1%"> End Date: </label>  <input type="text" placeholder="Pick Date" name="end_date" id="end_date">


                </p>
                <p align="center">
                    <button type="submit" value="Search" class="btn btn-large btn-danger " name="filterGridResult" id="filterGridResult">
                        <i class="fa fa-search" aria-hidden="true"></i>  Search by date
                    </button>
                </p>
            </form>
        </div>


    </div>
</div>
-->

<div class="row">

    <div class="col-sm-12">
        <div class="card card-mini">
            <div class="card-header">
                <div class="card-title">Individual Flagged Transactions </div>
                <ul class="card-action">

                </ul>
            </div>
            <div class="card-body no-padding table-responsive">
                <table class="table card-table">
                    <thead>
                    <tr>
                        <th><i class="fa fa-user" aria-hidden="true"></i>
                            Beneficiary</th>
                        <th>BVN</th>
                        <th>Amount Recieved</th>
                        <th>Flag Narrative</th>
                        <th>Transaction Date</th>
                        <th></th>



                    </tr>
                    </thead>
                    <tbody>

                    <?php

                    $num_rec_per_page=10;

                    if (isset($_GET["page"])) {
                        $page  = $_GET["page"];
                    } else {
                        $page=1;
                    }


                    $start_from = ($page-1) * $num_rec_per_page;
                    $result ="";
                  //  if(isset($_POST["filterGridResult"])){

                   // $start_date = new DateTime( $_POST['start_date'] );
                   // $end_date   = new DateTime( $_POST['end_date'] );
                    //$imto = $_POST["imto"];
                    //$bvn = $_POST["beneficiary_bvn"];


                    //DATE RANGE Only
                 //   if(isset($start_date) && isset($end_date) && $start_date!=null  && $end_date !=null ){


                    /**    $sql = "  SELECT * FROM imto_transactions
INNER JOIN individual_flag_log ON individual_flag_log.imto_transactions_id=imto_transactions.id
INNER JOIN imto ON imto_transactions.imto_id=imto.id
WHERE transaction_date BETWEEN :start_date AND :end_date ORDER BY transaction_date  ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
                    //  $sql = "SELECT * FROM imto_transactions  AND transaction_date BETWEEN :start_date AND :end_date  ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
                    $result = $db->prepare($sql);
                    $result->execute( array(
                    ":start_date" => $start_date->format( "Y-m-d" ),
                    ":end_date"   => $end_date->format( "Y-m-d" )
                    ));

                     **/
                        $sql="SELECT it.id, it.beneficiary_name, it.beneficiary_bvn, it.originating_currency_code,
it.originating_amount, it.transaction_date, it.transaction_ref, i.name, i.code, ifl.comment, ifl.flag_date
FROM imto_transactions it
INNER JOIN individual_flag_log ifl ON ifl.imto_transactions_id=it.id
INNER JOIN imto i ON it.imto_id=i.id
ORDER BY transaction_date DESC";
                        $result = $db->prepare($sql);
                        $result->execute();




                  //  }
                    if ($result->rowCount()){


                    ?>


                    <?php
                    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                        <tr>
                            <td> <?php echo $row['beneficiary_name'] ?> </td>
                            <td> <?php echo $row['beneficiary_bvn'] ?> </td>
                            <td> <?php echo $row['originating_currency_code'] . ' ' . number_format($row['originating_amount'], 2, '.', ','); ?>  </td>

                            <td><span class="badge badge-danger badge-icon" style="padding: 1%;"><i class="fa fa-flag" aria-hidden="true"></i><span><?php echo $row['comment']. ' On :'. $obj->returnYYYmmDDformattedDate($row['transaction_date']) ?></span></span></td>

                            <td> <?php echo $row['transaction_date'] ?> </td>

                            <td><a href='#'  class="btn btn-success">Unflag</a></td>
                            <!-- <td><a href='backend.php?cat=individual_flags&imto_transactions_id=<?php echo $row['id'] ?>'  class="btn btn-success">Unflag</a></td>-->

                        </tr>




                    <?php  }
                        }else{
                            echo "No transaction found";

                        }

                  //  }?>



                    </tbody>
                </table>


            </div>
        </div>
    </div>


</div>

