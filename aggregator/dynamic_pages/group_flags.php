

<div class="row">

    <div class="col-sm-12">
        <div class="card card-mini">
            <div class="card-header">
                <div class="card-title">Grouped Flagged Transactions </div>
                <ul class="card-action">

                </ul>
            </div>
            <div class="card-body no-padding table-responsive">
                <table class="table card-table">
                    <thead>
                    <tr>
                        <th><i class="fa fa-user" aria-hidden="true"></i>
                            Beneficiary</th>
                        <th>Group ID</th>
                        <th>Group Name</th>
                        <th>BVN</th>
                        <th>Amount Recieved</th>
                        <th>Flag Narratives</th>
                        <th>Transaction Purpose</th>
                        <th>Transaction Date</th>
                        <th></th>

                    </tr>
                    </thead>
                    <tbody>

                    <?php

                    $num_rec_per_page=10;

                    if (isset($_GET["page"])) {
                        $page  = $_GET["page"];
                    } else {
                        $page=1;
                    }


                    $start_from = ($page-1) * $num_rec_per_page;
                    $result ="";
                    //  if(isset($_POST["filterGridResult"])){

                    // $start_date = new DateTime( $_POST['start_date'] );
                    // $end_date   = new DateTime( $_POST['end_date'] );
                    //$imto = $_POST["imto"];
                    //$bvn = $_POST["beneficiary_bvn"];


                    //DATE RANGE Only
                    //   if(isset($start_date) && isset($end_date) && $start_date!=null  && $end_date !=null ){


                    /**    $sql = "  SELECT * FROM imto_transactions
                    INNER JOIN individual_flag_log ON individual_flag_log.imto_transactions_id=imto_transactions.id
                    INNER JOIN imto ON imto_transactions.imto_id=imto.id
                    WHERE transaction_date BETWEEN :start_date AND :end_date ORDER BY transaction_date  ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
                    //  $sql = "SELECT * FROM imto_transactions  AND transaction_date BETWEEN :start_date AND :end_date  ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
                    $result = $db->prepare($sql);
                    $result->execute( array(
                    ":start_date" => $start_date->format( "Y-m-d" ),
                    ":end_date"   => $end_date->format( "Y-m-d" )
                    ));

                     **/
                    $sql="SELECT *
FROM group_flag_log gfl
INNER JOIN group_flag gf ON gf.id = gfl.group_flag_id
INNER JOIN imto_transactions it ON it.id =gfl.imto_transactions_id
ORDER BY transaction_date DESC";
                    $result = $db->prepare($sql);
                    $result->execute();




                    //  }
                    if ($result->rowCount()){


                        ?>


                        <?php
                        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                            <tr>
                                <td> <?php echo $row['beneficiary_name'] ?> </td>

                                <td> <?php echo $row['group_name'] ?> </td>
                                <td> <?php echo $row['group_identifier'] ?> </td>

                                <td> <?php echo $row['beneficiary_bvn'] ?> </td>
                                <td> <?php echo $row['originating_currency_code'] . ' ' . number_format($row['originating_amount'], 2, '.', ','); ?>  </td>

                                <td><span class="badge badge-danger badge-icon" style="padding: 1%;"><i class="fa fa-flag" aria-hidden="true"></i><span><?php echo $row['comment']. ' On :'. $obj->returnYYYmmDDformattedDate($row['transaction_date']) ?></span></span></td>

                                <td> <?php echo $row['transaction_purpose'] ?> </td>

                                <td> <?php echo $row['transaction_date'] ?> </td>

                               <!-- <td><a href='backend.php?cat=individual_flags&imto_transactions_id=<?php echo $row['id'] ?>'  class="btn btn-success">Unflag</a></td>-->
                                <td><a href='#'  class="btn btn-success">Unflag</a></td>

                            </tr>




                        <?php  }
                    }else{
                        echo "No transaction found";

                    }

                    //  }?>



                    </tbody>
                </table>


            </div>
        </div>
    </div>


</div>

