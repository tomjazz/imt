

</body>
<!-- FOOTER-->


<div id="footer">
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <h2 align="center" class="logo" style="font-size: 24px; margin-top: 26%">generate4africa</h2>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <p align="center" style="color:white; margin-top: 26%"> Powered by Transaction Global Network. <?php echo date("Y"); ?> <br /> All Rights Reserved</p>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">

            <ul style="	margin-top: 26%;">
                <li>Home</li>
                <li>About Us</li>
                <li>charities</li>
                <li>Contact</li>
            </ul>

        </div>

    </div>
</div>



<script>
  /**  $(function ()
    {
        $("#wizard").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft"
        });
    });



    $(document).ready(function(e) {
        try {
            $("#head-content select").msDropDown();
        } catch(e) {
            alert(e.message);
        }
    });
    **/
</script>


<!-- ======================= JQuery libs =========================== -->
<!-- jQuery local-->
<script src="js/jquery.js"></script>

<script src="js/logic.js?ver<?php echo $nowDateTime?>"></script>

<script src="js/msdropdown/jquery.dd.js"></script>

<script src="js/FormSteps.js?ver<?php echo $nowDateTime?>"></script>
<script src="js/sweetalert.js?ver<?php echo $nowDateTime?>"></script>

<script type="text/javascript" src="js/bootstrap.js"></script>


<script src="js/card.js"></script>

</body>
</html>

