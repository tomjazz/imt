/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.7.14 : Database - imt
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`imt` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `imt`;

/*Table structure for table `banks` */

DROP TABLE IF EXISTS `banks`;

CREATE TABLE `banks` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `country_id` int(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `banks` */

insert  into `banks`(`id`,`name`,`country_id`,`code`) values (1,'GTB',75,'0'),(2,'UBA',75,'0'),(3,'FIRST BANK',75,'0'),(4,'BANK OF AMERICA',107,'0'),(5,'BANK OF ENGLAND',106,'0');

/*Table structure for table `cashout_channel` */

DROP TABLE IF EXISTS `cashout_channel`;

CREATE TABLE `cashout_channel` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `country_id` int(255) DEFAULT NULL,
  `service_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=227 DEFAULT CHARSET=latin1;

/*Data for the table `cashout_channel` */

insert  into `cashout_channel`(`id`,`country_id`,`service_id`) values (114,1,1),(115,2,1),(116,3,1),(117,4,1),(118,5,1),(119,6,1),(120,7,1),(121,8,1),(122,9,1),(123,10,1),(124,11,1),(125,12,1),(126,13,1),(127,14,1),(128,15,1),(129,16,1),(130,17,1),(131,18,1),(132,19,1),(133,20,1),(134,21,1),(135,22,1),(136,23,1),(137,24,1),(138,25,1),(139,26,1),(140,27,1),(141,28,1),(142,29,1),(143,30,1),(144,31,1),(145,32,1),(146,33,1),(147,34,1),(148,35,1),(149,36,1),(150,37,1),(151,38,1),(152,39,1),(153,40,1),(154,41,1),(155,42,1),(156,43,1),(157,44,1),(158,45,1),(159,46,1),(160,47,1),(161,48,1),(162,49,1),(163,50,1),(164,51,1),(165,52,1),(166,53,1),(167,54,1),(168,55,1),(169,56,1),(170,57,1),(171,58,1),(172,59,1),(173,60,1),(174,61,1),(175,62,1),(176,63,1),(177,64,1),(178,65,1),(179,66,1),(180,67,1),(181,68,1),(182,69,1),(183,70,1),(184,71,1),(185,72,1),(186,73,1),(187,74,1),(188,75,1),(189,76,1),(190,77,1),(191,78,1),(192,79,1),(193,80,1),(194,81,1),(195,82,1),(196,83,1),(197,84,1),(198,85,1),(199,86,1),(200,87,1),(201,88,1),(202,89,1),(203,90,1),(204,91,1),(205,92,1),(206,93,1),(207,94,1),(208,95,1),(209,96,1),(210,97,1),(211,98,1),(212,99,1),(213,100,1),(214,101,1),(215,102,1),(216,103,1),(217,104,1),(218,105,1),(219,106,1),(220,107,1),(221,108,1),(222,109,1),(223,110,1),(224,111,1),(225,112,1),(226,113,1);

/*Table structure for table `charity` */

DROP TABLE IF EXISTS `charity`;

CREATE TABLE `charity` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `country_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `charity` */

insert  into `charity`(`id`,`name`,`country_id`) values (1,'OKC Charity',75),(2,'Us Charity',107),(3,'Uk Charity',106),(4,'Convenant Charity',75),(5,'Osondu & Sons Charity',75);

/*Table structure for table `charity_rate` */

DROP TABLE IF EXISTS `charity_rate`;

CREATE TABLE `charity_rate` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `charge_value` decimal(30,16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `charity_rate` */

insert  into `charity_rate`(`id`,`name`,`charge_value`) values (1,'charity Charge','1.0000000000000000');

/*Table structure for table `country` */

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `currency_name` varchar(255) DEFAULT NULL,
  `currency_code` varchar(255) DEFAULT NULL,
  `isEnabled` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;

/*Data for the table `country` */

insert  into `country`(`id`,`name`,`currency_name`,`currency_code`,`isEnabled`) values (1,'Albania','Lek','ALL',0),(2,'Afghanistan','Afghani','AFN',0),(3,'Argentina','Peso','ARS',0),(4,'Aruba','Guilder','AWG',0),(5,'Australia','Dollar','AUD',0),(6,'Azerbaijan','Manat','AZN',0),(7,'Bahamas','Dollar','BSD',0),(8,'Barbados','Dollar','BBD',0),(9,'Belarus','Ruble','BYR',0),(10,'Belize','Dollar','BZD',0),(11,'Bermuda','Dollar','BMD',0),(12,'Bolivia','Boliviano','BOB',0),(13,'Bosnia and Herzegovina','Convertible Marka','BAM',0),(14,'Botswana','Pula','BWP',0),(15,'Bulgaria','Lev','BGN',0),(16,'Brazil','Real','BRL',0),(17,'Brunei','Darussalam Dollar','BND',0),(18,'Cambodia','Riel','KHR',0),(19,'Canada','Dollar','CAD',0),(20,'Cayman','Dollar','KYD',0),(21,'Chile','Peso','CLP',0),(22,'China','Yuan Renminbi','CNY',0),(23,'Colombia','Peso','COP',0),(24,'Costa Rica','Colon','CRC',0),(25,'Croatia','Kuna','HRK',0),(26,'Cuba','Peso','CUP',0),(27,'Czech Republic','Koruna','CZK',0),(28,'Denmark','Krone','DKK',0),(29,'Dominican Republic','Peso','DOP',0),(30,'East Caribbean','Dollar','XCD',0),(31,'Egypt','Pound','EGP',0),(32,'El Salvador','Colon','SVC',0),(33,'Estonia','Kroon','EEK',0),(34,'Euro Member','Euro','EUR',0),(35,'Falkland Islands','Pound','FKP',0),(36,'Fiji','Dollar','FJD',0),(37,'Georgia','Lari','GEL',0),(38,'Ghana','Cedis','GHC',0),(39,'Gibraltar','Pound','GIP',0),(40,'Guatemala','Quetzal','GTQ',0),(41,'Guernsey','Pound','GGP',0),(42,'Guyana','Dollar','GYD',0),(43,'Honduras','Lempira','HNL',0),(44,'Hong Kong','Dollar','HKD',0),(45,'Hungary','Forint','HUF',0),(46,'Iceland','Krona','ISK',0),(47,'India','Rupee','INR',0),(48,'Indonesia','Rupiah','IDR',0),(49,'Iran','Rial','IRR',0),(50,'Isle of Man','Pound','IMP',0),(51,'Israel','Shekel','ILS',0),(52,'Jamaica','Dollar','JMD',0),(53,'Japan','Yen','JPY',0),(54,'Jersey','Pound','JEP',0),(55,'Kazakhstan','Tenge','KZT',0),(56,'Korea (North)','Won','KPW',0),(57,'Korea (South)','Won','KRW',0),(58,'Kyrgyzstan','Som','KGS',0),(59,'Laos','Kip','LAK',0),(60,'Latvia','Lat','LVL',0),(61,'Lebanon','Pound','LBP',0),(62,'Liberia','Dollar','LRD',0),(63,'Lithuania','Litas','LTL',0),(64,'Macedonia','Denar','MKD',0),(65,'Malaysia','Ringgit','MYR',0),(66,'Mauritius','Rupee','MUR',0),(67,'Mexico','Peso','MXN',0),(68,'Mongolia','Tughrik','MNT',0),(69,'Mozambique','Metical','MZN',0),(70,'Namibia','Dollar','NAD',0),(71,'Nepal','Rupee','NPR',0),(72,'Netherlands','Antilles Guilder','ANG',0),(73,'New Zealand','Dollar','NZD',0),(74,'Nicaragua','Cordoba','NIO',0),(75,'Nigeria','Naira','NGN',1),(76,'Norway','Krone','NOK',0),(77,'Oman','Rial','OMR',0),(78,'Pakistan','Rupee','PKR',0),(79,'Panama','Balboa','PAB',0),(80,'Paraguay','Guarani','PYG',0),(81,'Peru','Nuevo Sol','PEN',0),(82,'Philippines','Peso','PHP',0),(83,'Poland','Zloty','PLN',0),(84,'Qatar','Riyal','QAR',0),(85,'Romania','New Leu','RON',0),(86,'Russia','Ruble','RUB',0),(87,'Saint Helena','Pound','SHP',0),(88,'Saudi Arabia','Riyal','SAR',0),(89,'Serbia','Dinar','RSD',0),(90,'Seychelles','Rupee','SCR',0),(91,'Singapore','Dollar','SGD',0),(92,'Solomon Islands','Dollar','SBD',0),(93,'Somalia','Shilling','SOS',0),(94,'South Africa','Rand','ZAR',0),(95,'Sri Lanka','Rupee','LKR',0),(96,'Sweden','Krona','SEK',0),(97,'Switzerland','Franc','CHF',0),(98,'Suriname','Dollar','SRD',0),(99,'Syria','Pound','SYP',0),(100,'Taiwan','New Dollar','TWD',0),(101,'Thailand','Baht','THB',0),(102,'Trinidad and Tobago','Dollar','TTD',0),(103,'Turkey','Lira','TRL',0),(104,'Tuvalu','Dollar','TVD',0),(105,'Ukraine','Hryvna','UAH',0),(106,'United Kingdom','Pound','GBP',1),(107,'United States','Dollar','USD',1),(108,'Uruguay','Peso','UYU',0),(109,'Uzbekistan','Som','UZS',0),(110,'Venezuela','Bolivar Fuerte','VEF',0),(111,'Viet Nam','Dong','VND',0),(112,'Yemen','Rial','YER',0),(113,'Zimbabwe','Dollar','ZWD',0);

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` int(255) DEFAULT '2',
  `country_id` int(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `customers` */

insert  into `customers`(`id`,`fname`,`lname`,`email`,`phone`,`password`,`role_id`,`country_id`,`created_at`,`updated_at`) values (1,'tomiwa','adebayo','tomiwa.adebayo@techadvance.ng','+2349094625113','cf640614d54c14ac4e91ea2ee198aaa06ae59ac7484a089c8de9c89c6a5d9afe',2,75,NULL,NULL),(2,'joke',NULL,'joke@gmail.com','03834','a2fa4cfad054c30de64ae9fd9807b2bb562eb12619b66d0bcf9629cd8c969fe0',2,75,NULL,NULL),(3,'Jide',NULL,'jidetest@gmail.com','0700','2a15147c4a61fc352ad62231e7af98233107dc99358be5aea6072d4dde591573',2,106,NULL,NULL),(4,'daniel',NULL,'danieltest@gmail.co','098909','bd3dae5fb91f88a4f0978222dfd58f59a124257cb081486387cbae9df11fb879',2,75,NULL,NULL);

/*Table structure for table `exchange_rate` */

DROP TABLE IF EXISTS `exchange_rate`;

CREATE TABLE `exchange_rate` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `convert_from` varchar(255) DEFAULT NULL,
  `convert_to` varchar(255) DEFAULT NULL,
  `resolved_value` decimal(30,10) DEFAULT NULL,
  `processing_fee` decimal(30,10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `exchange_rate` */

insert  into `exchange_rate`(`id`,`convert_from`,`convert_to`,`resolved_value`,`processing_fee`) values (1,'USD','GBP','0.7800000000','0.9900000000'),(2,'GBP','USD','1.2800000000','0.9900000000'),(3,'USD','NGN','305.5000000000','0.9900000000'),(4,'GBP','NGN','391.3500000000','0.9900000000'),(5,NULL,NULL,NULL,NULL);

/*Table structure for table `group_flag` */

DROP TABLE IF EXISTS `group_flag`;

CREATE TABLE `group_flag` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `group_flag` */

/*Table structure for table `group_flag_criteria` */

DROP TABLE IF EXISTS `group_flag_criteria`;

CREATE TABLE `group_flag_criteria` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `amount` decimal(30,16) DEFAULT NULL,
  `period` varchar(255) DEFAULT NULL,
  `period_value` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `group_flag_criteria` */

/*Table structure for table `group_flag_members` */

DROP TABLE IF EXISTS `group_flag_members`;

CREATE TABLE `group_flag_members` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `group_flag_id` int(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `bvn` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `group_flag_members` */

/*Table structure for table `imto` */

DROP TABLE IF EXISTS `imto`;

CREATE TABLE `imto` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `country_id` int(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `imto` */

insert  into `imto`(`id`,`name`,`code`,`country_id`) values (1,'Generate4Africa','G4A',75),(2,'Worldremit limited','WRL',75),(3,'Western Union','WU',75),(4,'MoneyGram','MG',75),(5,'UAE Exchange Center LLC','UECL',75),(6,'Wari limited','WL',75),(7,'Home Send','HS',75),(8,'World Financial Services Group','SWFSG',75),(9,'Weblink InternationaL','WIL',75),(10,'CP Express','CPE',75),(11,'DT&T Corporation','DTTC',75);

/*Table structure for table `imto_transactions` */

DROP TABLE IF EXISTS `imto_transactions`;

CREATE TABLE `imto_transactions` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `imto_id` int(255) DEFAULT NULL,
  `transaction_ref` varchar(255) DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `initiator_name` varchar(255) DEFAULT NULL,
  `originating_currency_code` varchar(255) DEFAULT NULL,
  `originating_amount` decimal(30,16) DEFAULT NULL,
  `processing_fee` decimal(30,16) DEFAULT NULL,
  `cummulative_fee` decimal(30,16) DEFAULT NULL,
  `destination_currency_code` varchar(255) DEFAULT NULL,
  `destination_state` varchar(255) DEFAULT NULL,
  `conversion_rate` decimal(30,16) DEFAULT NULL,
  `recieving_amount` decimal(30,16) DEFAULT NULL,
  `beneficiary_name` varchar(255) DEFAULT NULL,
  `beneficiary_email` varchar(255) DEFAULT NULL,
  `beneficiary_phone` varchar(255) DEFAULT NULL,
  `beneficiary_address` varchar(255) DEFAULT NULL,
  `beneficiary_bvn` varchar(255) DEFAULT NULL,
  `payment_channel` varchar(255) DEFAULT NULL,
  `recieving_channel` varchar(255) DEFAULT NULL,
  `isFlagged` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

/*Data for the table `imto_transactions` */

insert  into `imto_transactions`(`id`,`imto_id`,`transaction_ref`,`transaction_date`,`initiator_name`,`originating_currency_code`,`originating_amount`,`processing_fee`,`cummulative_fee`,`destination_currency_code`,`destination_state`,`conversion_rate`,`recieving_amount`,`beneficiary_name`,`beneficiary_email`,`beneficiary_phone`,`beneficiary_address`,`beneficiary_bvn`,`payment_channel`,`recieving_channel`,`isFlagged`) values (63,1,'G4A6aa7cba44ca72f','2017-05-14 05:21:00','Tomiwa Adebayo','USD','300.0000000000000000','0.0200000000000000','306.0000000000000000','NGN','Lagos','305.5000000000000000','91650.0000000000000000','Ebuka Daniel','ebuka@gmail.com','2.349E+11','no 26, idohan Street, Lagos Nigeria ','f4f867778678','Credit Card - Online','Bank Deposit -(GTB 1234567849)',0),(64,1,'G4A0708e551fb9d32','2017-05-14 05:21:36','Tomiwa Adebayo','GBP','450.0000000000000000','0.0200000000000000','459.0000000000000000','NGN','Lagos','391.3500000000000000','176107.5000000000000000','tope gabriael','tope.gabriel@gmail.com','90946253648','no 21c akinlade stree, victoria Island Lagos ','v7v7vv7777','Credit Card - Online','Bank Deposit -(GTB 1234567849)',0),(65,1,'G4Ad73da94c954324','2017-05-14 05:21:40','Tomiwa Adebayo','USD','120.0000000000000000','2.4000000000000000','122.4000000000000000','NGN','Lagos','305.5000000000000000','36660.0000000000000000','Ejike Daniel','ejike@daniels.com','8.05364E+11','Ejike\'s court Lagos','0303038383d','Credit Card - Online','Bank Deposit -(GTB 1234567849)',0),(66,1,'G4Ab528684feee74e','2017-05-14 05:21:45','Tomiwa Adebayo','GBP','500.0000000000000000','10.0000000000000000','510.0000000000000000','NGN','Lagos','391.3500000000000000','195675.0000000000000000','Tola Akinlade','tola.akinlade@gmail.com','2.34908E+11','akinsewe stree, lagos Island Lagos ','5479fb39f739','Credit Card - Online','Bank Deposit -(GTB 1234567849)',0),(67,2,'WR8c378574e8f348','2017-05-14 05:21:49','Tomiwa Adebayo','GBP','60.0000000000000000','1.2000000000000000','61.2000000000000000','NGN','Lagos','391.3500000000000000','23481.0000000000000000','gbenga','gbenga@gmai.com','8080','hfgjdhjf ','4f78f7f4f748','Credit Card - Online','Bank Deposit -(GTB 1234567849)',0),(68,2,'WR0cf67c60005fb2','2017-05-14 05:21:52','Tomiwa Adebayo','GBP','500.0000000000000000','10.0000000000000000','510.0000000000000000','NGN','Lagos','391.3500000000000000','195675.0000000000000000','tiwa savage','tiwa.savage@gmail.com','7063847281','Kilani Street, Victorial Island Lagos Nigeria','g585b8b89859','Credit Card - Online','Bank Deposit -(GTB 1234567849)',0),(69,3,'WU747e70ea28a6b1','2017-05-14 05:23:09','Tomiwa Adebayo','GBP','200.0000000000000000','4.0000000000000000','204.0000000000000000','NGN','Lagos','391.3500000000000000','78270.0000000000000000','Kolade Lawani','kola.lawani@gmail.com','70046278374','No 21, kolade street, Lagos Nigeria ','92xbdd3f3f4f44','Credit Card - Online','Bank Deposit -(GTB 1234567849)',0),(70,3,'WU5bf3f5f4fc5363','2017-05-14 05:23:13','Tomiwa Adebayo','GBP','400.0000000000000000','8.0000000000000000','408.0000000000000000','NGN','Lagos','391.3500000000000000','156540.0000000000000000','jide osikoya','jide.osikoya@gmail.com','2.34909E+12','no 12, Osikoya street, Lagos Nigeria ','4784b92bf38f49','Credit Card - Online','Bank Deposit -(GTB 1234567849)',0),(71,1,'G4b3a7df3fdcbbc7','2017-05-14 05:23:16','Tomiwa Adebayo','GBP','300.0000000000000000','6.0000000000000000','306.0000000000000000','NGN','Lagos','391.3500000000000000','117405.0000000000000000','dele sanwo Iyayi','sanwo@gmail.com','808435678','no 21 delesanwo, victorial island, lagos ','bf49023y9ub993','Credit Card - Online','Bank Deposit -(GTB 1234567849)',0),(72,1,'G42de366a3c91f24','2017-05-14 05:23:19','Tomiwa Adebayo','GBP','180.0000000000000000','3.6000000000000000','183.6000000000000000','NGN','Lagos','391.3500000000000000','70443.0000000000000000','Bolajoko Tanimowo','bolajoko@gmail.com','2.34909E+12','no 5 daribere street, off ita alamu vi lagos ','fyv83yf3f3yf33','Credit Card - Online','Bank Deposit -(GTB 1234567849)',0),(73,4,'MG8021966267f902','2017-05-14 05:23:19','Tomiwa Adebayo','GBP','400.0000000000000000','8.0000000000000000','408.0000000000000000','NGN','Lagos','391.3500000000000000','156540.0000000000000000','esu','esu@gmail.com','987','8478yufb','48ybfieubfyif9','Credit Card - Online','Bank Deposit -(GTB 1234567849)',0);

/*Table structure for table `kiosks` */

DROP TABLE IF EXISTS `kiosks`;

CREATE TABLE `kiosks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `channel_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kiosks` */

/*Table structure for table `operators` */

DROP TABLE IF EXISTS `operators`;

CREATE TABLE `operators` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` int(255) DEFAULT '2',
  `country_id` int(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `operators` */

/*Table structure for table `payment_channel` */

DROP TABLE IF EXISTS `payment_channel`;

CREATE TABLE `payment_channel` (
  `id` int(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `payment_channel` */

insert  into `payment_channel`(`id`,`name`) values (1,'Stripe'),(2,'PayPal'),(3,'Venmo'),(4,'Dwolla');

/*Table structure for table `processing_fee_rate` */

DROP TABLE IF EXISTS `processing_fee_rate`;

CREATE TABLE `processing_fee_rate` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `charge_value` decimal(30,16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `processing_fee_rate` */

insert  into `processing_fee_rate`(`id`,`name`,`charge_value`) values (1,'processing fee','2.0000000000000000');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `role_type` varchar(255) DEFAULT NULL,
  `role_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`id`,`role_type`,`role_code`) values (1,'ADMIN','-1'),(2,'CUSTOMER','2'),(3,'CBN','3');

/*Table structure for table `services` */

DROP TABLE IF EXISTS `services`;

CREATE TABLE `services` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `services` */

insert  into `services`(`id`,`name`) values (1,'Direct Bank Deposit'),(2,'SMS');

/*Table structure for table `singular_flag_criteria` */

DROP TABLE IF EXISTS `singular_flag_criteria`;

CREATE TABLE `singular_flag_criteria` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `amount` decimal(30,16) DEFAULT NULL,
  `period` varchar(255) DEFAULT NULL,
  `period_value` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `singular_flag_criteria` */

/*Table structure for table `singular_flag_members` */

DROP TABLE IF EXISTS `singular_flag_members`;

CREATE TABLE `singular_flag_members` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `bvn` varchar(255) DEFAULT NULL,
  `comments` varchar(1000) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `singular_flag_members` */

/*Table structure for table `transactions` */

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `transaction_ref` varchar(255) DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `customer_id` int(255) DEFAULT NULL,
  `originating_country_id` int(255) DEFAULT NULL,
  `originating_currency_code` varchar(255) DEFAULT NULL,
  `originating_amount` decimal(30,16) DEFAULT NULL,
  `processing_fee` decimal(30,16) DEFAULT NULL,
  `cummulative_fee` decimal(30,16) DEFAULT NULL,
  `payment_channel` varchar(255) DEFAULT NULL,
  `destination_country_id` varchar(255) DEFAULT NULL,
  `destination_currency_code` varchar(255) DEFAULT NULL,
  `conversion_rate` decimal(30,16) DEFAULT NULL,
  `recieving_amount` decimal(30,16) DEFAULT NULL,
  `beneficiary_name` varchar(255) DEFAULT NULL,
  `beneficiary_email` varchar(255) DEFAULT NULL,
  `beneficiary_phone` varchar(255) DEFAULT NULL,
  `beneficiary_address` varchar(255) DEFAULT NULL,
  `transaction_status` varchar(255) DEFAULT NULL,
  `status_update_date` datetime DEFAULT NULL,
  `payment_channel_id` int(255) DEFAULT NULL,
  `charity_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

/*Data for the table `transactions` */

insert  into `transactions`(`id`,`transaction_ref`,`transaction_date`,`customer_id`,`originating_country_id`,`originating_currency_code`,`originating_amount`,`processing_fee`,`cummulative_fee`,`payment_channel`,`destination_country_id`,`destination_currency_code`,`conversion_rate`,`recieving_amount`,`beneficiary_name`,`beneficiary_email`,`beneficiary_phone`,`beneficiary_address`,`transaction_status`,`status_update_date`,`payment_channel_id`,`charity_id`) values (43,'G4A6aa7cba44ca72f','2017-04-25 11:57:28',1,107,'USD','300.0000000000000000','0.0200000000000000','306.0000000000000000',NULL,'75','NGN','305.5000000000000000','91650.0000000000000000','Ebuka Daniel','ebuka@gmail.com','+234900000000','no 26, idohan Street, Lagos Nigeria ','COMPLETED','2017-04-25 11:57:28',1,4),(45,'G4A0708e551fb9d32','2017-04-25 12:00:41',1,106,'GBP','450.0000000000000000','0.0200000000000000','459.0000000000000000',NULL,'75','NGN','391.3500000000000000','176107.5000000000000000','tope gabriael','tope.gabriel@gmail.com','090946253648','no 21c akinlade stree, victoria Island Lagos ','COMPLETED','2017-04-25 12:00:41',1,5),(46,'G4Ad73da94c954324','2017-04-25 12:05:13',1,107,'USD','120.0000000000000000','2.4000000000000000','122.4000000000000000',NULL,'75','NGN','305.5000000000000000','36660.0000000000000000','Ejike Daniel','ejike@daniels.com','0805363728271','Ejike\'s court Lagos','COMPLETED','2017-04-25 12:05:13',1,4),(47,'G4Ab528684feee74e','2017-04-25 14:22:01',1,106,'GBP','500.0000000000000000','10.0000000000000000','510.0000000000000000',NULL,'75','NGN','391.3500000000000000','195675.0000000000000000','Tola Akinlade','tola.akinlade@gmail.com','+234908487382','akinsewe stree, lagos Island Lagos ','COMPLETED','2017-04-25 14:22:01',1,5),(48,'G4A8c378574e8f348','2017-04-26 12:11:42',1,106,'GBP','60.0000000000000000','1.2000000000000000','61.2000000000000000',NULL,'75','NGN','391.3500000000000000','23481.0000000000000000','gbenga','gbenga@gmai.com','08080','hfgjdhjf ','COMPLETED','2017-04-26 12:11:42',1,1),(51,'G4A0cf67c60005fb2','2017-04-26 12:16:19',1,106,'GBP','500.0000000000000000','10.0000000000000000','510.0000000000000000',NULL,'75','NGN','391.3500000000000000','195675.0000000000000000','tiwa savage','tiwa.savage@gmail.com','07063847281','Kilani Street, Victorial Island Lagos Nigeria','COMPLETED','2017-04-26 12:16:19',1,5),(52,'G4A747e70ea28a6b1','2017-04-26 12:20:28',1,106,'GBP','200.0000000000000000','4.0000000000000000','204.0000000000000000',NULL,'75','NGN','391.3500000000000000','78270.0000000000000000','Kolade Lawani','kola.lawani@gmail.com','070046278374','No 21, kolade street, Lagos Nigeria ','COMPLETED','2017-04-26 12:20:28',1,4),(53,'G4A5bf3f5f4fc5363','2017-04-26 12:24:31',1,106,'GBP','400.0000000000000000','8.0000000000000000','408.0000000000000000',NULL,'75','NGN','391.3500000000000000','156540.0000000000000000','jide osikoya','jide.osikoya@gmail.com','+2349094627338','no 12, Osikoya street, Lagos Nigeria ','COMPLETED','2017-04-26 12:24:31',1,5),(54,'G4Ab3a7df3fdcbbc7','2017-04-26 12:32:02',1,106,'GBP','300.0000000000000000','6.0000000000000000','306.0000000000000000',NULL,'75','NGN','391.3500000000000000','117405.0000000000000000','dele sanwo Iyayi','sanwo@gmail.com','0808435678','no 21 delesanwo, victorial island, lagos ','COMPLETED','2017-04-26 12:32:02',1,4),(60,'G4A2de366a3c91f24','2017-04-26 13:55:02',1,106,'GBP','180.0000000000000000','3.6000000000000000','183.6000000000000000',NULL,'75','NGN','391.3500000000000000','70443.0000000000000000','Bolajoko Tanimowo','bolajoko@gmail.com','+2349094673123','no 5 daribere street, off ita alamu vi lagos ','COMPLETED','2017-04-26 13:55:02',1,4),(61,'G4A8021966267f902','2017-05-03 13:31:17',4,106,'GBP','400.0000000000000000','8.0000000000000000','408.0000000000000000',NULL,'75','NGN','391.3500000000000000','156540.0000000000000000','esu','esu@gmail.com','0987','8478yufb','COMPLETED','2017-05-03 13:31:17',1,4);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
