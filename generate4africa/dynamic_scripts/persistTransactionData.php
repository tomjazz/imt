<?php
session_start();
define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__.'/Repo.php');

//require_once(__ROOT__.'/methodmain2-peb-email-3625118b2417/send-email2.php');

//$emailObj = new SSMTP();

if(isset($_POST['action'])) {
    $_SESSION['ex'] = $_POST['action'];

    $repoObj = new Repo();

    $nowDateTime = new DateTime('NOW');
    $nowDateTime = $nowDateTime->format('Y-m-d H:i:s');
    $transaction_date = $nowDateTime;
        $transaction_ref = "G4A" . $repoObj->randomString();
       $_SESSION['transaction_ref']= $transaction_ref;

    $db = $repoObj->getConnection();


    $result = $db->prepare("SELECT * FROM transactions WHERE transaction_ref=:transaction_ref");
    $result->execute([
        "transaction_ref" => $_SESSION['transaction_ref']
    ]);

    if ($result->rowCount()) {


        return false;
    } else {

        $result = $db->prepare("
INSERT INTO transactions SET
transaction_ref=:transaction_ref,
transaction_date=:transaction_date,
customer_id=:customer_id,
originating_country_id=:originating_country_id,
originating_currency_code=:originating_currency_code,
originating_amount=:originating_amount,
processing_fee=:processing_fee,
cummulative_fee=:cummulative_fee,
destination_country_id=:destination_country_id,
destination_currency_code=:destination_currency_code,
conversion_rate=:conversion_rate,
recieving_amount=:recieving_amount,
beneficiary_name=:beneficiary_name,
beneficiary_phone=:beneficiary_phone,
beneficiary_email=:beneficiary_email,
transaction_status=:transaction_status,
status_update_date=:status_update_date,
beneficiary_address=:beneficiary_address,
charity_id=:charity_id,
payment_channel_id=:payment_channel_id
");
        $result->execute([
            'transaction_ref' => $transaction_ref,
            'transaction_date' => $transaction_date,
            'customer_id' => $_SESSION['user_id'],
            'originating_country_id' => $repoObj->returnIdOfSendingCountry(),
            'originating_currency_code' => $_SESSION['from_currency'],
            'originating_amount' =>$repoObj->cleanData($_SESSION['transfer_amount']),
            'processing_fee' => $repoObj->cleanData($_SESSION['processing_fee_computed']),
            'cummulative_fee' => $repoObj->cleanData($_SESSION['cumulativeFees']),
            'destination_country_id' => $repoObj->returnIdOfRecievingCountry(),
            'destination_currency_code' => $_SESSION['to_currency'],
            'conversion_rate' => $repoObj->cleanData($_SESSION['base_conversion']),
            'recieving_amount' => $repoObj->cleanData($_SESSION['receive_amount']),
            'beneficiary_name' => $_SESSION['beneficiary_name'],
            'beneficiary_phone' => $_SESSION['beneficiary_phone'],
            'beneficiary_email' => $_SESSION['beneficiary_email'],
            'transaction_status' => "COMPLETED",
            'status_update_date' => $transaction_date,
            'beneficiary_address' =>$_SESSION['beneficiary_address'],
            'charity_id' => $_SESSION['charity_id'],
            'payment_channel_id' =>'1']
        );

        if ($result) {
            //$emailObj->sendMailOnSuccessfulTransferViaAws();

            echo true;
        } else {

            echo false;

        }


    }

}








