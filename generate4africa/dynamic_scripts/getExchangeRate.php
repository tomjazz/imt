
<?php
session_start();
ob_start();



    define('__ROOT__', dirname(dirname(__FILE__)));
    require_once(__ROOT__ . '/Repo.php');

    if (isset($_POST['from_currency']) && isset($_POST['to_currency']) && isset($_POST['transfer_amount'])) {

        $repoObj = new Repo();
        $db = $repoObj->getConnection();


        $_SESSION['from_currency'] = $_POST['from_currency'];
        $_SESSION['to_currency'] = $_POST['to_currency'];
        $_SESSION['transfer_amount'] = number_format($_POST['transfer_amount'], 2, '.', ',');







                $result = $db->prepare("SELECT * FROM charity WHERE id=:id");
                $result->execute([
                    "id" => $_POST['charity_id']
                ]);

                if ($result->rowCount()) {


                    while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {

                        $_SESSION['charity_id'] =  $charity_id = $rows['id'];
                        $_SESSION['charity_name'] =  $charity_name = $rows['name'];





            }
        }




        $result = $db->prepare("SELECT * FROM processing_fee_rate");
        $result->execute();

        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {

                $_SESSION['processing_fee'] = $rows['charge_value']/100;

            }
        }


        $result = $db->prepare("SELECT * FROM charity_rate");
        $result->execute();

        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {

               // $charity_fee = number_format($rows['charge_value'], 2, '.', ',');
                $_SESSION['charity_fee'] = $rows['charge_value'];

            }
        }




        $result = $db->prepare("SELECT * FROM country WHERE currency_code=:from_currency_code");
        $result->execute([
            "from_currency_code" => $_SESSION['from_currency']
        ]);

        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {

                $_SESSION['sender_country_id'] = $rows['id'];
                $_SESSION['sender_country_name'] = $rows['name'];


            }
        }


        $result = $db->prepare("SELECT * FROM country WHERE currency_code=:currency_code");
        $result->execute([
            "currency_code" => $_SESSION['to_currency']
        ]);

        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {

                $_SESSION['beneficiary_country_id'] = $rows['id'];
                $_SESSION['beneficiary_country_name'] = $rows['name'];


            }
        }


        $sql = "SELECT * FROM exchange_rate WHERE convert_from=:convert_from AND convert_to=:convert_to ";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(":convert_from", $_SESSION['from_currency']);
        $stmt->bindValue(":convert_to", $_SESSION['to_currency']);

        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result > 0) {

            $processing_fee = number_format( $_SESSION['processing_fee'] * $_POST['transfer_amount'] , 2, '.', ',');
            $cumulativeFees = number_format( $_SESSION['processing_fee'] * $_POST['transfer_amount'] + $_POST['transfer_amount'], 2, '.', ',');

            $receive_amount = number_format($result[0]['resolved_value'] * $_POST['transfer_amount'], 2, '.', ',');
            $base_conversion = number_format($result[0]['resolved_value'], 2, '.', ',');


            $charity_fee_computed  = number_format( ($result[0]['resolved_value']) * ($_SESSION['charity_fee']/100)  *  ($_SESSION['processing_fee'] * $_POST['transfer_amount']) , 2, '.', ',');


            $arr = array($_SESSION['transfer_amount'],$processing_fee, $cumulativeFees, $receive_amount, $base_conversion, $charity_fee_computed, $charity_name,  number_format($_SESSION['charity_fee'], 2, '.', ','));



            $_SESSION['cumulativeFees'] = $cumulativeFees;
            $_SESSION['receive_amount'] = $receive_amount;
            $_SESSION['base_conversion'] = $base_conversion;

            $_SESSION['processing_fee_computed'] = $processing_fee;
            $_SESSION['charity_fee_computed'] = $charity_fee_computed;





            echo json_encode($arr);
        } else {
            return false;
        }

    } else {
    };



?>

