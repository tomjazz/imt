<?php
session_start();
ob_start();

define('__ROOT__', dirname(dirname(__FILE__)));
require_once(__ROOT__ . '/Repo.php');

if (isset($_POST['country_code'])) {

    $repoObj = new Repo();
    $db = $repoObj->getConnection();


    $result = $db->prepare("SELECT * FROM country WHERE currency_code=:currency_code");
    $result->execute([
        "currency_code" => $_POST['country_code']
    ]);

    if ($result->rowCount()) {

        while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {

           $country_id = $rows['id'];

        }
    }


    $sql = "SELECT * FROM charity WHERE country_id=:country_id";
    $stmt = $db->prepare($sql);
    $stmt->bindValue(":country_id", $country_id);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if ($result > 0) {



        echo json_encode($result);
    } else {
        return false;
    }

} else {
};



?>

