<?php

/**
 * Created by PhpStorm.
 * User: Tomiwa
 * Date: 4/26/2017
 * Time: 2:05 PM
 **/
define('__ROOT__', dirname(dirname(__FILE__)));
    require_once(__ROOT__ . '/methodmain2-peb-email-3625118b2417/send-email2.php');

if (isset($_POST['fire'])) {
    $emailObj = new SSMTP();
    $emailObj->sendMailOnSuccessfulTransferViaAws();

    unset($_SESSION['sender_country_name']);
    unset($_SESSION['beneficiary_name']);
    unset($_SESSION['beneficiary_phone']);
    unset($_SESSION['beneficiary_email']);
    unset($_SESSION['beneficiary_country_name']);
    unset($_SESSION['beneficiary_address']);
    unset($_SESSION['from_currency']);
    unset($_SESSION['to_currency']);
    unset($_SESSION['transfer_amount']);
    unset($_SESSION['processing_fee_computed']);
    unset($_SESSION['cumulativeFees']);
    unset($_SESSION['receive_amount']);
    unset($_SESSION['charity_name']);
    unset($_SESSION['charity_fee_computed']);
    unset($_SESSION['charity_fee']);
    unset($_SESSION['base_conversion']);

}