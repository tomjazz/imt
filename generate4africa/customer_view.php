<?php
include 'header2.php';
//session_start();
//define('__ROOT__', dirname(dirname(__FILE__)));
//include('Repo.php');

              if (!isset($_SESSION["user_id"])) {
                  header('Location:index.php');}else
                  $userid = $_SESSION['user_id'];

//echo $_SESSION['user_id']; exit();

//$repo = new Repo();
$db = $ReboObj->getConnection();


?>




<div id="icon-dashedboard">
    <div class="container" align="" style="margin-left: 100px">


        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
            <p align="center">
                <a href="index" style="color:#222; text-decoration: none">
                    <img src="img/dash-icon1.png">  <br />
                    Send money
                </a>
            </p>
        </div>



        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
            <p align="center">
                <a href="#" style="color:#222; text-decoration: none">
                    <img src="img/dash-icon2.png">  <br />
                    Pay bill
                </a>
            </p>
        </div>

        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
            <p align="center">
                <a href="#" style="color:#222; text-decoration: none">
                    <img src="img/dash-icon4.png">  <br />
                    Find pay location
                </a>
            </p>
        </div>

        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
            <p align="center">
                <a href="#" style="color:#222; text-decoration: none">
                    <img src="img/dash-icon5.png">  <br />
                    pay at pay location
                </a>
            </p>
        </div>

        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
            <p align="center">
                <a href="customer_view" style="color:#222; text-decoration: none">
                    <img src="img/dash-icon6.png">  <br />
                    transaction history
                </a>
            </p>
        </div>

    </div>
</div>


<hr / style="border-top:2px solid #eee">


<div id="head-content" style="padding-top: 2.5%; min-height:500px;">

    <div class="container">



        <?php

        $num_rec_per_page=5;

        if (isset($_GET["page"])) {
            $page  = $_GET["page"];
        } else {
            $page=1;
        }


        $start_from = ($page-1) * $num_rec_per_page;
        if(isset($_POST["CustomerBookingSearchBox"])){
            $searchParam =$_POST["CustomerBookingSearchBox"];
            $sql = "SELECT * FROM transactions WHERE customer_id=:user_id AND transaction_ref=:CustomerBookingSearchBox ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
            $result = $db->prepare($sql);
            $result->bindValue(":user_id",$userid);
            $result->bindValue(":CustomerBookingSearchBox",$searchParam);
            $result->execute();
        }else {
            $sql = "SELECT * FROM transactions WHERE customer_id=:user_id ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
            $result = $db->prepare($sql);
            $result->bindValue(":user_id", $userid);
            $result->execute();
        }
        if ($result->rowCount()){


        ?>

        <div class="search-box">

            <h3 align="center">Search Filter</h3>

            <form method="post">
                <p align="center">
                    <input type="text" placeholder="Start date">
                    <input type="text" placeholder="End date">



                </p>
                <p align="center">
                    <input type="text"  id="CustomerBookingSearchBox"  name="CustomerBookingSearchBox" placeholder="Input reference number"  class="form-control" style="height: 49px; left: 0px; width: 53%; padding: 2.4%; background: #d8d7d7; border:none; border-radius: 40px; font-size: 20px; font-family: 'Raleway', arial;"> <br>

                </p>
                <p align="center">
                    <input type="submit"  id='filterCustomerBookingGridResult' name="filterCustomerBookingGridResult" value="Filter"  class="btn btn-default" style="background: #2f82b2; border: none; width: 20%">

                </p>
            </form>

        </div>

        <table class="table" style="font-size: 18px;">
            <thead>
            <tr>
                <th >Transaction Ref</th>
                <th>Transaction Date </th>
                <th>Beneficiary Name</th>
                <th>Sending Currency</th>
                <th>Transfer Amount</th>

                <th>Recieving Currency</th>
                <th>Recieving Amount</th>
                <th>Transaction Status</th>

            </tr>
            </thead>

            <?php
            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            ?>
            <tbody>
            <tr>
                <td lass="tg-i81m" ><?php echo $row['transaction_ref'];?></td>
            <td lass="tg-i81m" ><?php echo $row['transaction_date']; ?></td>
            <td lass="tg-i81m" ><?php echo $row['beneficiary_name']; ?></td>
            <td lass="tg-i81m" ><?php echo $row['originating_currency_code'];?></td>
            <td lass="tg-i81m" ><?php echo number_format($row['originating_amount'], 2, '.', ','); ?> </td>

            <td lass="tg-i81m" ><?php echo $row['destination_currency_code'];?></td>
            <td lass="tg-i81m" ><?php echo number_format($row['recieving_amount'], 2, '.', ',');?></td>
            <td lass="tg-i81m" ><?php echo $row['transaction_status']; ?></td>
            </tr>
            <?php
            }
            }else{
                echo "No transaction found";

            }
            ?>
            </tbody>
        </table>

        <?php

        if(isset($_POST["CustomerBookingSearchBox"])) {

            $sql = "SELECT * FROM transactions WHERE customer_id=:user_id AND transaction_ref=:CustomerBookingSearchBox ORDER BY transaction_date DESC LIMIT $start_from, $num_rec_per_page ";
            $result = $db->prepare($sql);
            $result->bindValue(":user_id", $userid);
            $result->bindValue(":CustomerBookingSearchBox", $_POST["CustomerBookingSearchBox"]);
            $result->execute();
            $rs_result = $result->fetchAll(PDO::FETCH_ASSOC);
            $total_records = count($rs_result);  //count number of records
            $total_pages = ceil($total_records / $num_rec_per_page);
        }else {

            $sql = "SELECT * FROM transactions WHERE customer_id=:user_id ORDER BY transaction_date DESC";
            $result = $db->prepare($sql);
            $result->bindValue(":user_id", $userid);
            $result->execute();

            $rs_result = $result->fetchAll(PDO::FETCH_ASSOC);
            $total_records = count($rs_result);  //count number of records
            $total_pages = ceil($total_records / $num_rec_per_page);
        }

        if($rs_result) {


            echo "<div align=''>
         <a href='profile.php?page=1'>" . '|<' . "</a> "; // Goto 1st page

            for ($i = 1; $i <= $total_pages; $i++) {
                echo "<a class='nav_pagination' href='customer_view.php?page=" . $i . "'>" . $i . "</a>";
            };
            echo "<a href='customer_view.php?page=$total_pages'>" . '>|' . "</a>
        </div>"; // Goto last page
        }
        ?>


    </div>
</div>

<hr />




<div class="content-central">

    <div class="content_info">
        <!-- title-vertical-line-->
        <div class="title-vertical-line">

            <div class="shell-wide" style="border: 1px solid; margin: auto;  background:;">
<div class="range range-xs-center offset-md-top-95">





<div id="">

    <h4 class="">  </h4>








<?php include 'footer.php' ?>

