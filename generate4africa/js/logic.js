//$.noConflict();
$('.form').trigger("reset");

$(document ).on( 'focus', ':input', function(){
    $(this ).attr('autocomplete', 'off');
});

function isNumberKey(txt, evt) {

    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 46) {
        //Check if the text already contains the . character
        if (txt.value.indexOf('.') === -1) {
            return true;
        } else {
            return false;
        }
    } else {
        if (charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;
    }
    return true;
}


$('#beneficiaryModalClose').click(function(){

    $('#transferModal').modal('hide');

});


$('#transfer_modal_back').click(function(){

    alert("he");
});



function validateTransferForm(){

    if($("#sendFromField").val() =="" || $("#sendFromField").val() ==null ){
        swal(
            'Oops...',
            'Send-From field has to be filled ',
            'error'
        );

        //$('#transferModalFire').hide();
        $('.stepwizard-step').hide();

        // $('.stepwizard-step').attr('disabled',true);

        $('#step-1').show();

        $('#step-2').hide();
        $('#step-3').hide();
        return false;
    }

    if($("#sendToField").val() =="" || $("#sendToField").val() ==null ){
        swal(
            'Oops...',
            'Send-To field has to be filled ',
            'error'
        );
        $('#step-1').show();
        $('#step-2').hide();
        $('#step-3').hide();
        return false;
    }

    if($("#transfer_amount").val() =="" || $("#transfer_amount").val() ==null ){
        swal(
            'Oops...',
            'Amount field has to be filled ',
            'error'
        );
        $('#step-1').show();
        $('#step-2').hide();
        $('#step-3').hide();
        return false;
    }

    $('.stepwizard-step').show();

    return false;

}





$('#transferModalFire').click(function(){

   // alert("here ooooo");

    $.ajax({
        type : 'POST',
        url : 'dynamic_scripts/getExchangeRate.php', //Here you should put your query
        data: {from_currency:  $("#sendFromField").val(), to_currency: $("#sendToField").val(), transfer_amount:$("#transfer_amount").val(), charity_id:$("#charityField").val()},
        success : function(fdata){

          //  alert(fdata);
           // $('#cont').html(fdata);
            var opts = $.parseJSON(fdata);
            // Use jQuery's each to iterate over the opts value

          //  console.log(fdata);


            // $.each(opts, function(i, d) {

            $('#send_amount').html(opts[0]);
            $('.send_amount_currency_code').html($("#sendFromField").val());
            $('.recieve_amount_currency_code').html($("#sendToField").val());


            $('#processing_fees').html(opts[1]);
            $('#total').html(opts[2]);
            $('#receive_amount').html(opts[3]);
            $('#base_conversion').html(opts[4]);
            $('#charity_fee_computed').html(opts[5]);
            $('#charity_name').html(opts[6]);
            $('#charity_fee').html(opts[7]);

            //  });
        }
    });

});




$('#onfirmationPageFire').click(function(){

    // $.post("dynamic_scripts/process_beneficiary_data.php", {beneficiary_email: $("#beneficiary_email").val()});

    //alert(id);;
    $.ajax({
        type : 'POST',
        url : 'dynamic_scripts/process_beneficiary_data.php', //Here you should put your query
        data: {beneficiary_name:  $("#beneficiary_name").val(), beneficiary_phone: $("#beneficiary_phone").val(), beneficiary_email:$("#beneficiary_email").val(), beneficiary_address:$("#beneficiary_address").val()},
        success : function(fdata){

            window.location.href = "Confirmation.php";

            //  });
        }
    });

});


function validateEmail(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}


function validateBeneficiaryForm(){

    if($("#beneficiary_name").val() =="" || $("#beneficiary_name").val() ==null ){
        swal(
            'Oops...',
            'Beneficiary Name cannot be empty',
            'error'
        );
        return false;

    }




    if($("#beneficiary_phone").val() =="" || $("#beneficiary_phone").val() ==null ){
        swal(
            'Oops...',
            'Beneficiary Phone cannot be empty',
            'error'
        );
        return false;

    }





    if($("#beneficiary_email").val() =="" || $("#beneficiary_email").val() ==null || !validateEmail($("#beneficiary_email").val()) ){
        swal(
            'Oops...',
            'Invalid Email Supplied',
            'error'
        );
        return false;

    }





    if($("#beneficiary_address").val() =="" || $("#beneficiary_address").val() ==null ){
        swal(
            'Oops...',
            'Beneficiary Address cannot be empty',
            'error'
        );

        return false;

    }



    $.ajax({
        type : 'POST',
        url : 'dynamic_scripts/process_beneficiary_data.php', //Here you should put your query
        data: {beneficiary_name:  $("#beneficiary_name").val(), beneficiary_phone: $("#beneficiary_phone").val(), beneficiary_email:$("#beneficiary_email").val(), beneficiary_address:$("#beneficiary_address").val()},
        success : function(fdata){

            window.location.href = "signUp.php";

            //  });
        }
    });

     return false;

}








function validateCardFields(){

    if($("#card_number").val() =="" || $("#card_number").val() ==null ){
        swal(
            'Oops...',
            'Card Number Not Supplied',
            'error'
        );
        return false;
    }

    if($("#expiry").val() =="" || $("#expiry").val() ==null ){
        swal(
            'Oops...',
            'Expiry Date Not Supplied',
            'error'
        );
        return false;
    }

    if($("#cvc").val() =="" || $("#cvc").val() ==null ){
        swal(
            'Oops...',
            'cvc Not Supplied',
            'error'
        );
        return false;
    }

   // return false;
}


function validateSignUpField(){


    if($("#name").val() =="" || $("#name").val() ==null ){
        swal(
            'Oops...',
            'name field  cannot be empty',
            'error'
        );
        return false;
    }

    if($("#email2").val() =="" || $("#email2").val() ==null  || !validateEmail($("#email2").val())){
        swal(
            'Oops...',
            'invalid email supplied',
            'error'
        );
        return false;
    }

    if($("#phone_number").val() =="" || $("#phone_number").val() ==null ){
        swal(
            'Oops...',
            'phone field  cannot be empty',
            'error'
        );
        return false;
    }

    if($("#password2").val() =="" || $("#password2").val() ==null ){
        swal(
            'Oops...',
            'password field  cannot be empty',
            'error'
        );
        return false;
    }

    if($("#password_confirmation").val() =="" || $("#password_confirmation").val() ==null ){
        swal(
            'Oops...',
            'password confirmation field  cannot be empty',
            'error'
        );
        return false;
    }


    if($("#password2").val() != $("#password_confirmation").val() ){
        swal(
            'Oops...',
            'password entered does not match',
            'error'
        );
        return false;
    }

    if($("#country").val()=="" ||$("#country").val()==null){
        swal(
            'Oops...',
            'country field cannot be empty',
            'error'
        );
        return false;
    }
}


function validateLoginField(){

    if($("#email").val() =="" || $("#email").val() ==null ){
        swal(
            'Oops...',
            'email field  cannot be empty',
            'error'
        );
        return false;
    }

    if($("#password").val() =="" || $("#password").val() ==null ){
        swal(
            'Oops...',
            'password field  cannot be empty',
            'error'
        );
        return false;
    }

}


function fetchCharityOfRecievingCountry() {

    $('.charityField').empty()
    var charityField = document.getElementById("charityField");
    var country_code = sendToField.options[sendToField.selectedIndex].value;


    $.ajax({
        type: "POST",
        url: "dynamic_scripts/FetchCharities.php",
        data: { 'country_code': country_code  },

        success: function(data){
            //  console.log(data);
            // Parse the returned json data
            var opts = $.parseJSON(data);
            // Use jQuery's each to iterate over the opts value
            $.each(opts, function(i, d) {

              //  console.log(data);

                $('.charityField').append('<option value="' + d.id+ '">' + d.name + '</option>');

            });
        }
    });
}


function fetchRecievingCountry() {

    $('.sendToField').empty()
    var sendToField = document.getElementById("sendToField");
    var country_code = sendFromField.options[sendFromField.selectedIndex].value;

   var openComma="(";
    var closeComma=")";

    $.ajax({
        type: "POST",
        url: "dynamic_scripts/FetchRecievingCountry.php",
        data: { 'country_code': country_code  },

        success: function(data){
            //  console.log(data);
            // Parse the returned json data
            var opts = $.parseJSON(data);
            // Use jQuery's each to iterate over the opts value
            $('.sendToField').append('<option value=""> --------------Select-------------- </option>');

            $.each(opts, function(i, d) {

                //  console.log(data);

              //  console.log( $('.sendToField').append('<option value="' + d.currency_code+ '">' + d.name + ' (' + d.currency_code + ')</option>'));

                $('.sendToField').append('<option value="' + d.currency_code+ '">' + d.name + ' (' + d.currency_code + ')</option>');

            });
        }
    });
}


