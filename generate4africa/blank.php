<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/msdropdown/jquery.dd.js"></script>
<link rel="stylesheet" type="text/css" href="css/dd.css" />

<link rel="stylesheet" type="text/css" href="css/msdropdown/skin2.css" />
<link rel="stylesheet" type="text/css" href="css/msdropdown/flags.css" />
<link href="https://fonts.googleapis.com/css?family=Catamaran:200|Poppins:300|Raleway:200|Roboto:500|Ubuntu:300" rel="stylesheet">


	<script type="text/javascript" src="js/bootstrap.js"></script>
	<title>Generate4Africa </title>
</head>
<body>

<div id="header2" style="min-height: 0px; padding-bottom: 0px;">
	<div id="head">
		<div class="container">
			<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
				<div id="nav">
					<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li ><a href="#">about us </a></li>
        <li><a href="#">charity</a></li>
        <li><a href="#">contact</a></li>
        
      </ul>
    </div>
    </div>
    </div>
    </nav>
	</div>
	</div>



			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<h2 align="center" class="logo">generate4africa</h2>
			</div>

			<div class="col-lg-4 col-md-3 hidden-sm hidden-xs ">
		 
				
			
			</div>
		</div> 
	</div>
	</div>
	<div id="icon-dashedboard">
		<div class="container">
			<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
				<p align="center">
				<a href="page.php" style="color:#222; text-decoration: none">
						<img src="img/dash-icon1.png">  <br />
						Send money
				</a>
				</p>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
				<p align="center">
				<a href="payment.php" style="color:#222; text-decoration: none">
				<img src="img/dash-icon2.png">  <br />
				Pay bill
				</a>
				</p>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
				<p align="center">
				<a href="#" style="color:#222; text-decoration: none">
				<img src="img/dash-icon4.png">  <br />
				Track Transfer
				</a>
				</p>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
				<p align="center">
				<a href="#" style="color:#222; text-decoration: none">
				<img src="img/dash-icon4.png">  <br />
				Find pay location
				</a>
				</p>
			</div>

			<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
				<p align="center">
				<a href="receiver.php" style="color:#222; text-decoration: none">
				<img src="img/dash-icon5.png">  <br />
				pay at pay location
				</a>
				</p>
			</div>

			<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
				<p align="center">
				<a href="table.php" style="color:#222; text-decoration: none">
				<img src="img/dash-icon6.png">  <br />
				transaction history
				</a>
				</p>
			</div>
		</div>
	</div>

		<hr / style="border-top:2px solid #eee">


	<div id="head-content" style="padding-top: 2.5%; height:300px;">

			<div class="container">


				

			</div>
</div>
</div>

<hr />



<div id="footer">
	<div class="container">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<h2 align="center" class="logo" style="font-size: 24px; margin-top: 26%">generate4africa</h2>	
		</div>

		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		<p align="center" style="color:white; margin-top: 26%"> Powered by Transaction Global Network. <?php echo date("Y"); ?> <br /> All Rights Reserved</p>
		</div>

		<div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">

			<ul style="	margin-top: 26%;">
				<li>Home</li>
				<li>About Us</li>
				<li>charities</li>
				<li>Contact</li>
			</ul>

		</div>

	</div>
</div>


<script type="text/javascript">
	$(document).ready(function(e) {
try {
$("#head-content select").msDropDown();
} catch(e) {
alert(e.message);
}
});
      </script>

</body>
</html>