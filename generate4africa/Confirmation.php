<?php
include 'header.php' ;


if(!isset($_SESSION['sender_country_name']) ||
    !isset($_SESSION['user_id']) ||

    !isset($_SESSION['beneficiary_name']) ||
    !isset($_SESSION['beneficiary_phone']) ||
    !isset($_SESSION['beneficiary_email']) ||
    !isset($_SESSION['beneficiary_country_name']) ||
    !isset($_SESSION['beneficiary_address']) ||
    !isset($_SESSION['from_currency']) ||
    !isset($_SESSION['to_currency']) ||
    !isset($_SESSION['transfer_amount']) ||
    !isset($_SESSION['processing_fee_computed']) ||
    !isset($_SESSION['cumulativeFees']) ||
    !isset($_SESSION['receive_amount']) ||
    !isset($_SESSION['charity_name']) ||
    !isset($_SESSION['charity_fee_computed']) ||
    !isset($_SESSION['charity_fee']) ||
    !isset($_SESSION['base_conversion'])

){
    header('location:index.php');
}




?>



<div class="container" style="    background: #fff; width: 75%;">

    <br><br><br>
    <div align="center">
        <h2 align="center" style="font-weight: bold">TRANSFER CONFIRMATION</h2>
    </div> <br><br><br>




    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 " >

        <div class="mgs" style="min-height: 377px;">

            <h2 align="center" style="font-size: 18px;"> details</h2>

            <ul>

                <li><span> <strong style="font-size: 13px;">Sender Name :</strong>   <span class=""></span>   <span id=""></span> <span id=""> <?php echo  $_SESSION['fname'] .' '. $_SESSION['lname'] ?></span>  </span> <br>

                <li><span> <strong style="font-size: 13px;">Sender Country :</strong>   <span class=""></span>   <span id=""></span> <span id=""> <?php echo  $_SESSION['sender_country_name'] ?></span>  </span> <br>

                <li><span> <strong style="font-size: 13px;">Beneficiary Name :</strong>   <span class=""></span>   <span id=""></span> <span id=""> <?php echo  $_SESSION['beneficiary_name'] ?></span>  </span> <br>

                <li><span> <strong style="font-size: 13px;">Beneficiary Phone :</strong>   <span class=""></span>   <span id=""></span> <span id=""> <?php echo  $_SESSION['beneficiary_phone'] ?></span>  </span> <br>

                <li><span> <strong style="font-size: 13px;">Beneficiary Email :</strong>   <span class=""></span>   <span id=""></span> <span id=""> <?php echo  $_SESSION['beneficiary_email'] ?></span>  </span> <br>

                <li><span> <strong style="font-size: 13px;">Beneficiary Country :</strong>   <span class=""></span>   <span id=""></span> <span id=""> <?php echo  $_SESSION['beneficiary_country_name'] ?></span>  </span> <br>


                <li><span> <strong style="font-size: 13px;">Beneficiary Address :</strong>   <span class=""></span>   <span id=""></span> <span id="charity_name"> <?php echo  $_SESSION['beneficiary_address'] ?></span>  </span> <br>


                <li><span> <strong style="font-size: 13px;">Send amount :</strong>   <span class="send_amount_currency_code">  <?php echo  $_SESSION['from_currency'] ?> </span>   <span id="send_amount"></span>  <?php echo  $_SESSION['transfer_amount'] ?> </span> <br>

                <li><span> <strong style="font-size: 13px;">Processing fee :</strong>   <span class="send_amount_currency_code"> <?php echo  $_SESSION['from_currency'] ?> </span>   <span id="processing_fees"></span>  <?php echo   $_SESSION['processing_fee_computed'] ?>  </span> <br>


                <li><span> <strong style="font-size: 13px;">Total to pay :</strong>   <span class="send_amount_currency_code"> <?php echo  $_SESSION['from_currency'] ?>  </span>   <span id="total"> <?php echo   $_SESSION['cumulativeFees'] ?>  </span>  </span> <br>

                <li><span> <strong style="font-size: 13px;">Recipient gets :</strong>   <span class="recieve_amount_currency_code">  <?php echo  $_SESSION['to_currency'] ?> </span>   <span id="receive_amount">   <?php echo  $_SESSION['receive_amount'] ?></span>  </span> <br>

                <li><span> <strong style="font-size: 13px;">Charity :</strong>   <span class=""></span>   <span id=""></span> <span id="charity_name"> <?php echo  $_SESSION['charity_name'] ?></span>  </span> <br>


                <li><span> <strong style="font-size: 13px;">Amount to charity :</strong>   <span class="recieve_amount_currency_code"> <?php echo  $_SESSION['to_currency'] ?></span>   <span id="charity_fee_computed"> <?php echo   $_SESSION['charity_fee_computed'] ?></span>  </span> <br>
</li>
                </ul>

            <br>
            <br>
            <i> <span style="font-size: 13px; font-weight: bold; color: #0b0000"> * Charity Gets  <span class=""></span> <span id="charity_fee"><?php echo  number_format($_SESSION['charity_fee'], 2, '.', ','); ?> </span>  % of our processing fee <br></span> </span></i>


            <i> <span style="font-size: 13px; font-weight: bold; color: #0b0000""> * Exchange rate :- 1 <span class="send_amount_currency_code"> <?php echo  $_SESSION['from_currency'] ?></span>  = <span class="recieve_amount_currency_code"> <?php echo  $_SESSION['to_currency'] ?></span> <span id="base_conversion"> <?php echo  $_SESSION['base_conversion'] ?></span> <br></span></i>









        </div>



    </div>

    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12" style="">
        <div class="cal-div" style="box-shadow: none; height: auto">







            <div id="payment-select" style="float: eft; height: 724px;"  >

                <!--<div class="container-fluid" align="center">
                    <h4 align="center" style="color:#113984; font-style: 16px; margin-bottom: 10%">Select payment</h4>


                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="border:1px solid #eee; border-radius: 10px;"  id="strip_div_fire">
                        <p align="center">
                            <a href="#"><img src="img/stripe-logo.png" width="80%" style="padding: 5%"></a>
                        </p>
                    </div>


                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="border:1px solid #eee; border-radius: 10px;" id="paypal_div_fire">
                        <p align="center">
                            <a href="#" ><img src="img/paypal_adaptive.png" width="75%" style="padding: 9.5%"></a>
                        </p>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border:1px solid #eee; border-radius: 10px; margin-top: 2%" id="dwolla_div_fire">
                        <p align="center">
                            <a href="#" ><img src="img/dwolla.png" width="60%" style="padding: 3%"></a>
                        </p>
                    </div>
                </div>
                -->



                <div class="row" id="paymentLinkDiv" align="center">
                    <strong> <h3>Select Payment Option</h3> </strong>
<br>
                    <a href="#" id="strip_div_fire" > <img src="img/stripe-logo.png" width="250" /> </a> <br><br><br>
                    <a href="#"  id="paypal_div_fire" ><img src="img/paypal_adaptive.png"  width="250" /> <br><br><br>
                    <a href="#" id="dwolla_div_fire"><img src="img/dwolla.png"  width="250" /> </a>
                </div>





            <!-- Payment Options-->




                <br> <br>

                <div id="stripe_card_div" style="display:none;max-width: 315px; margin: auto; border: 1px solid #dddddd; border-radius: 10px;" >

                    <div class="card-wrapper" style=""></div>

                    <form method="post"  onsubmit="return validateCardFields()"  class="form-theme" id="pay_form">
                        <input placeholder="Card number" type="tel" name="number" id="card_number" class="form-control">
                        <input placeholder="MM/YYYY" type="text" name="expiry" id="expiry" class="form-control">
                        <input placeholder="CVC" type="text" name="cvc" id="cvc"  class="form-control">


                        <br>


                        <div align="center">

                            <a href="#" class="ModalBackBtn btn btn-danger pb"  id="strip_div_close" style=" width: 40%; padding: 2%;
                             background: #3081b1;   border-radius: 30px; color: #fff; font-family: runmedium; font-size: 20px;
                                margin-top: 5%; margin-right: 7%; border: 0px;" >Cancel </a>

                            <input type="submit" value="pay" id="pay" name="pay" class="btn btn-danger pb" />




                        </div>

                        <br>
                        <br>

                    </form>

                </div>
            </div>





        </div>
    </div>
    <p> &nbsp</p>
    <p> &nbsp</p>
    <p> &nbsp</p>
    <p> &nbsp</p>
    <p> &nbsp</p>
    <p> &nbsp</p>

</div>



<?php include 'footer.php' ?>

<div style="margin-bottom: -150px;"> </div>




<script language="javascript">


    var sampleCard ="5500 0000 0000 0004";
    var expiry ="12/2020";
    var cvc ="856";

    new Card({
        form: document.querySelector('form'),
        container: '.card-wrapper',

        placeholders: {
            number: '5500 0000 0000 0004',
            name: 'TEST CARD',
            expiry: '12/2020',
            cvc: '856'
        },
        messages: {
            // monthYear: 'mm/yyyy' // optional - default 'month/year'
        }
    });




    $( document ).ready(function() {


        $("#paypal_div_fire").click(function(e){
            e.preventDefault();

        });

        $("#dwolla_div_fire").click(function(e){
            e.preventDefault();

        });

        $("#strip_div_fire").click(function(e){
            e.preventDefault();

            $("#paypal_div_fire").hide();
            $("#dwolla_div_fire").hide();
            $("#stripe_card_div").slideDown(2000);


        });


        $("#strip_div_close").click(function(e){
            e.preventDefault();
            $("#paypal_div_fire").show();
            $("#dwolla_div_fire").show();
            $("#stripe_card_div").slideUp(2000);


        });

        //$("#card_number").on("change paste keyup", function() {

        $('#pay_form').on('submit', function(e){
            e.preventDefault();

            var expiry_value = $("#expiry").val().replace(/ /g, "");

            // alert(expiry_value);




            if($("#card_number").val()==sampleCard && expiry_value == expiry && $('#cvc').val()==cvc){
                //$('#card_holder_name').val("TEST CARD").change();
                // alert("correct ooo");

                //call ajax for persisting all transaction data

                $.ajax({
                    method: "POST",
                    url: "dynamic_scripts/persistTransactionData.php",
                    data: { action: "action" }
                }).done(function(data) {

                    //alert(data);


                    if(data == 1){

                        $.post("dynamic_scripts/Email.php", {fire: "fire"});

                        swal('Correct', 'Your Money Transfer Was Successful', 'success');
                        setTimeout(function(){// wait for 2 secs(2)
                            window.location.href = "customer_view.php";
                        }, 1000);
                    } else {

                        swal('Oops...','It appears that this transaction already exist','error');
                        setTimeout(function(){// wait for 2 secs(2)
                            window.location.href = "customer_view.php";
                        }, 1000);
                    }


                });



            }
            else{

                swal('Oops...','Invalid Card Details','error');
            }



        });



//});






    });


</script>

