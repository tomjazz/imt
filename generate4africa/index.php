<?php include "header.php";?>

	<div id="head-content">

			<div class="container">

				<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12" >

					<div id="left-div" style="margin-left: -5%">
					<h2>Send Money, <br /> Fast & Secure</h2>
					<p>generate4africa is a quick and easy money transfer platform that allows you to send funds across to loved ones, for business and other personal or work related services. <br/><br/>
                        Like our name suggests we donate a percentage of our service charge to a selected charity of your choice, helping to generate for Africa.
                    </p>
					</div>

					<div class="line-box"></div>

				</div>


				<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
					<div class="cal-div" style="box-shadow: none; height: auto">



                        <div id="wizard" style="width: 100%">

                            <br>

                           <div class="stepwizard col-md-offset-0" style="width: 100%; display: none;">

                                <div class="stepwizard-row setup-panel">
                                    <div class="stepwizard-step">
                                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                        <p>Transfer Details</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                        <p>Summary</p>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                                        <p>Beneficiary Details</p>
                                    </div>
                                </div>

                            </div>

                            <br>







                            <div class="row setup-content" id="step-1" style="display: block; padding: 6px 5px; margin-right: 50px; margin-left: 40px;">



                                    <div class="col-xs-10 col-md-offset-1" style="width: 100%; margin-left: 0px">
                                        <form method="post" onsubmit="return validateTransferForm()"  class="form-theme">



                                            <label class="control-label">Send From</label><br>
                                            <select  class="frm-control sendFromField" name="sendFromField" id="sendFromField" onchange="fetchRecievingCountry()"  style=" left: 0px; width: 100%; adding: 2.4%; background: #d8d7d7; border:none; border-radius: 40px; font-size: 20px; font-family: 'Raleway', arial;">
                                            <option value="">--------------Select--------------</option>

                                            <?php foreach ($sendCountries  as $value => $row) :  ?>
                                                <option value="<?php echo $row['currency_code'] ?>"> <?php echo $row['name']." (". $row['currency_code']." )" ?> </option> <?php endforeach; ?>
                                        </select> <br> <br>  <br>



                                        <label class="control-label">Send To</label><br>
                                        <select  class="form-control sendToField" name="sendToField" id="sendToField"  onchange="fetchCharityOfRecievingCountry()"  style="height: 49px; left: 0px; width: 100%; adding: 2.4%; background: #d8d7d7; border:none; border-radius: 40px; font-size: 20px; font-family: 'Raleway', arial;">
                                        </select> <br> <br>  <br>



                                        <label class="control-label">Select Charity</label><br>
                                        <select  class="form-control charityField" name="charityField" id="charityField"   style="height: 49px; left: 0px; width: 100%; padding: 2.4%; background: #d8d7d7; border:none; border-radius: 40px; font-size: 20px; font-family: 'Raleway', arial;">
                                            <!-- <option value="">--------------Select--------------</option>-->
                                        </select> <br> <br>  <br>


                                            <label class="control-label">Amount</label><br>
                                        <input  type="text" id="transfer_amount" name="transfer_amount" placeholder="Enter value" class="form-control" onkeypress="return isNumberKey(this,event);"   style="height: 49px; left: 0px; width: 100%; padding: 2.4%; background: #d8d7d7; border:none; border-radius: 40px; font-size: 20px; font-family: 'Raleway', arial;">
                                        <br> <br>



                                       <div align="center">
                                           <input type="submit" value="Next" name="start_transfer"  id="transferModalFire" class="btn btn-default nextBtn">


                                       </div>
                                        </form>

                                    </div>
                                </div>


                            <div class="row setup-content" id="step-2" style="display: block; padding: 6px 5px; margin-right: 50px; margin-left: 40px;">
                                <div class="col-xs-10 col-md-offset-1" style="width: 100%; margin-left: 0px">


                                    <div align="center">
                                        <h2 align="center" style="font-weight: bold">SUMMARY</h2>
                                    </div> <br>


                                        <div align="center">
                                            <div align="left" id="cont">
                                                <span> <strong style="font-size: 18px;">Send amount :</strong>   <span class="send_amount_currency_code"></span>   <span id="send_amount"></span>  </span> <br><br>

                                                <span> <strong style="font-size: 18px;">Processing fee :</strong>   <span class="send_amount_currency_code"></span>   <span id="processing_fees"></span>  </span> <br><br>


                                                <span> <strong style="font-size: 18px;">Total to pay :</strong>   <span class="send_amount_currency_code"></span>   <span id="total"></span>  </span> <br><br>

                                                <span> <strong style="font-size: 18px;">Recipient gets :</strong>   <span class="recieve_amount_currency_code"></span>   <span id="receive_amount"></span>  </span> <br><br>

                                                <span> <strong style="font-size: 18px;">Charity :</strong>   <span class=""></span>   <span id=""></span> <span id="charity_name"></span>  </span> <br><br>


                                                <span> <strong style="font-size: 18px;">Amount to charity :</strong>   <span class="recieve_amount_currency_code"></span>   <span id="charity_fee_computed"></span>  </span> <br><br>




                                                <br>
                                                <i> <span style="font-size: 12px; font-weight: bold; color: #ff0000"> * Charity Gets  <span class=""></span> <span id="charity_fee"> </span>  % of our processing fee <br></span> </span></i>


                                                <i> <span style="font-size: 12px; font-weight: bold; color: #ff0000""> * Exchange rate :- 1 <span class="send_amount_currency_code"></span>  = <span class="recieve_amount_currency_code"></span> <span id="base_conversion"></span> <br></span></i>


                                            </div>

                                        </div>

                                        <br>



                                    <div align="center">
                                        <input type="submit" value="Previous"  class="btn btn-default prevBtn btn-lg pull-left">
                                        <input type="submit" value="Next" data-toggle="modal" name="beneficiaryModalFire" data-target="#beneficiaryModal" id="beneficiaryModalFire" class="btn btn-default nextBtn pull-right">

                                    </div>
                                        <br>


                                </div>



                                </div>



                            <div class="row setup-content" id="step-3" style="display: block; padding: 6px 5px; margin-right: 50px; margin-left: 40px;">

                                <div align="center">
                                    <h2 align="center" style="font-weight: bold">BENEFICIARY DETAILS</h2>
                                </div> <br>

                                <div class="col-xs-10 col-md-offset-1" style="width: 100%; margin-left: 0px">
                                    <form method="post" onsubmit="return validateBeneficiaryForm()"  class="form-theme">


                                        <label>Beneficiary Name</label>

                                        <input type="text" laceholder="" name="beneficiary_name" id="beneficiary_name" class="form-control" style="height: 49px; left: 0px; width: 100%; padding: 2.4%; background: #d8d7d7; border:none; border-radius: 40px; font-size: 20px; font-family: 'Raleway', arial;"> <br>

                                        <label>Beneficiary Phone</label>
                                        <input type="tel" laceholder="Beneficiary Phone" name="beneficiary_phone" id="beneficiary_phone" class="form-control"  style="height: 49px; left: 0px; width: 100%; padding: 2.4%; background: #d8d7d7; border:none; border-radius: 40px; font-size: 20px; font-family: 'Raleway', arial;"> <br>

                                        <label>Beneficiary Email</label>
                                        <input type="text" laceholder="Beneficiary Email" name="beneficiary_email" id="beneficiary_email" class="form-control"  style="height: 49px; left: 0px; width: 100%; padding: 2.4%; background: #d8d7d7; border:none; border-radius: 40px; font-size: 20px; font-family: 'Raleway', arial;"> <br>

                                        <label>Beneficiary Address</label>
                                        <input type="text" laceholder="Beneficiary Address" name="beneficiary_address" id="beneficiary_address" class="form-control"  style="height: 49px; left: 0px; width: 100%; padding: 2.4%; background: #d8d7d7; border:none; border-radius: 40px; font-size: 20px; font-family: 'Raleway', arial;"> <br>




                                        <div align="center">
                                            <input type="button" value="Previous"  class="btn btn-default prevBtn btn-lg pull-left"
                                                   style=" width: 40%; padding: 2%;  background: #3081b1;   border-radius: 30px; color: #fff; font-family: runmedium; font-size: 20px;
                                                 margin-top: 5%; margin-right: 7%; ">

                                            <?php
                                            if(!isset($_SESSION['user_id'])){
                                                echo'<a href="signUp.php" class="btn btn-default nextBtn pull-right"  onclick="return validateBeneficiaryForm()"
                                                 style=" width: 40%; padding: 2%;  background: #3081b1;   border-radius: 30px; color: #fff; font-family: runmedium; font-size: 20px;
                                                 margin-top: 5%; margin-right: 7%; border: 0px; ">Next<a>';
                                            }
                                            else {
                                                echo '
                                            <input type="submit" value="Next" data-toggle="modal" name="" id="confirmationPageFire" class="btn btn-default nextBtn pull-right">';
                                            }


                                            ?>

                                        </div>





                                    </form>

                                </div>
                            </div>




                        </div>



					</div>
				</div>



			</div>

	</div>


</div>


<!-- EASY TO USE SECTIONS-->

<div id="icon-base">

    <div class="container">

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<span class="icon-span">
		<img src="img/icon1.png"> <br />
			<p align="center">fast <br />service</p>
		</span>
		<span class="icon-span">
		<img src="img/icon2.png">
		 <br />
			<p align="center">easy <br />to use</p>

		</span>
		<span class="icon-span">
		<img src="img/icon3.png">
		<br />
			<p align="center">donation <br />platform</p>
		</span>

            <div class="clear-fix"></div>
            <hr style="border-top: 5px solid #e9e9e9;" />
        </div>


        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <h2 align="center">Minimum charge of <br /> $3.00 per transaction</h2>
            <p align="center">Cause we don't charge you a percentage on your <br /> transaction, you are guaranteed to always have fair rates with us.s</p>
        </div>

    </div>

</div>




<!-- DOWNLOAD OUR APP SECTION-->

<div id="down-base">

<div class="container">

	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h2>Download our mobile <br />
app today and  pay with <br />ease</h2>
<p>Download our mobile app today and get a 10% <br />
discount towards your first transaction. Download <br /> now from the Play Store and IOS Store.</p>


	</div>

    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

    <?php if(!isset($_SESSION["user_id"])){?>
        <button type="button" class="btn btn-primary sign-in-base" data-toggle="modal" data-target=".bs-example-modal-sm" > Sign in</button>
        <a href="signUp.php" class="sign-up-base btn btn-primary sign-in-base"> Sign Up</a>

    <?php }?>

        </div>

	<div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
		<img src="img/mobile-phone.png" width="80%">
	</div>
</div>

</div>




<!-- CHARITY SECTION-->

<div id="down-base" style="margin-top: 0%; background:white">

    <div class="container">

        <h2 align="center" style="font-size: 30px;">Charities</h2>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="c-box">
                <img src="img/cleanwater.png">
                <h3>Clean Water Initiative</h3>
                <p style="font-size: 16px;">Providing clean water to  <br />

                    millions in Africa.</p>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="c-box">
                <img src="img/maca.png" style="padding-right: 7%">
                <h3>Mothers Against Child<br/> Abandonement</h3>
                <p style="font-size: 16px">Creating a  platfform for support

                    <br />

                    and prevention of teenage pregnancies. </p>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style=" font-family: arial">
            <img src="img/ablechild.png" style="float: left; padding-right: 8%">
            <h3>Able Child Africa</h3>
            <p style="font-size: 16px">Providing equal rights <br/>

                for disabled children and <br />

                young people.</p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style=" font-family: arial">

            <img src="img/vita.png" style="float: left; padding-right: 8%">
            <h3>Vita</h3>
            <p style="font-size: 16px;">Providing food and shelter to the <br/>

                less privilaged in Africa as well as <br/>

                campaigning against polution.</p>
        </div>

    </div>

</div>


<?php include "footer.php";?>