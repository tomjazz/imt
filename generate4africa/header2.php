
<?php
session_start();
ob_start();

$nowDateTime = new DateTime('NOW');
$nowDateTime = $nowDateTime->format('Y-m-d H:i:s');

include "BusinessLogic.php";
//include "Repo.php";
//if(isset($_SESSION['username'])){
//    header('location:CustomerView.php');
//}

$obj = new BusinessLogic();
$obj->router();

$ReboObj = new Repo();
$sendCountries = $ReboObj->returnSendCountry();
$recievingCountries = $ReboObj->returnRecievingCountry();




?>





<!DOCTYPE html>
<html>
<head>




    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="css/dd.css" />
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css?ver<?php echo $nowDateTime?>">
    <!--<link rel="stylesheet" href="css/jquery.steps.css"> -->
    <link rel="stylesheet" type="text/css" href="css/msdropdown/skin2.css" />
    <link rel="stylesheet" type="text/css" href="css/msdropdown/flags.css" />
    <link rel="stylesheet" href="css/sweetalert.css">
    <link rel="stylesheet" href="css/FormSteps.css">



    <link href="https://fonts.googleapis.com/css?family=Catamaran:200|Poppins:300|Raleway:200|Roboto:500|Ubuntu:300" rel="stylesheet">




    <title>Generate4Africa </title>
</head>
<body>

<div class="row" align="center" style="background: black; padding: 10px;">
    <div class="form-theme">
        <?php if(isset($_SESSION["user_id"])){?>

            <div class="reveal-inline-block text-middle"><a class="show1" href="logout.php" ><span class="unit-left text-middle"><span class="icon icon-xxs icon-warning icon-circle mdi mdi-login"></span></span><span class="unit-body"><span class="text-warning" style="color: #2f82b2;">Hi,  <a href="customer_view"  style="color: #2f82b2;"> <?php echo $_SESSION['email']?></a>   |  <a href="customer_view" style="color: #2f82b2;"> View Profile</a>   </div>

        <?php }?>
    </div>
</div>

<div id="header2" style="min-height: 0px; padding-bottom: 0px;">
    <div id="head">
        <div class="container">

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div id="nav">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="width: 531px;">
                                    <ul class="nav navbar-nav">
                                        <li ><a href="index">Home </a></li>
                                        <li ><a href="#">about us </a></li>
                                        <li><a href="#">charity</a></li>
                                        <li><a href="#">contact</a></li>

                                    </ul>
                                </div>
                            </div>
                    </nav>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <h2 align="center" class="logo">generate4africa</h2>
            </div>


            <div class="col-lg-4 col-md-4 hidden-sm hidden-xs ">
                <p align="right" style="margin: 22px 0 10px;">
                    <?php if(!isset($_SESSION["user_id"])){?>    <button class="right-h sign-uph" class="btn btn-primary sign-in-base" data-toggle="modal" data-target=".bs-example-modal-sm" style="background:none; border:none" > login</button> <?php }?>
                    <?php if(!isset($_SESSION["user_id"])){?>    <a href="signUp" class="right-h sign-uph" class="btn btn-primary sign-in-base"  style="background:none;">Sign up</a><?php
                    }else{?>
                        <a href="logout.php" class="right-h sign-uph" class="btn btn-primary sign-in-base" style="background:none;"> &nbsp;&nbsp;  Log Out &nbsp; &nbsp; </a>
                    <?php }?>

                </p>
            </div>





        </div>
    </div>
</div>


    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="padding:10%;border-radius:50px">

                <div id="login-div">

                    <form method="post" onsubmit="return validateLoginField()" id="loginForm">

                        <label>username </label> <br />
                        <input type="text" placeholder="email" name="email" id="email" class="input">

                        <label>password </label> <br />
                        <input type="password" placeholder="Password" name="password" id="password" class="input">

                        <p align="center">
                            <input type="submit" class="sign-in-base" value="sign in" name="auth" style="color:#2f82b2; font-size: 25px">
                        </p>

                        <hr / style="border-top:2px solid #333">


                    </form>
                </div>

            </div>
        </div>
    </div>

