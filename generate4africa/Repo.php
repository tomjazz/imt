
<?php


//define('__ROOT__', dirname(dirname(__FILE__)));
//require_once(__ROOT__ . '/Repo.php');

include "__.php";


class Repo extends __
{

    public $db;


    public function __construct()
    {
        new __();

        try {
            $conn = new PDO("mysql:host=" . $this->dbh() . ";dbname=" . $this->dbn(), $this->dbu(), $this->dbp());
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        $this->db = $conn;

    }


    public function getConnection()
    {
        return $this->db;
    }


    public function randomString(){
        $random = bin2hex(openssl_random_pseudo_bytes(7));
        return $random;
    }



    function cleanData($a) {

       $var = floatval(preg_replace('/[^\d.]/', '', $a));

        return $var;

    }





    public function createUser($name,$email, $phone_number, $password, $country){
        $db = $this->getConnection();
        $sql = "SELECT * FROM customers WHERE email=:email";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(":email",$email);
        $stmt->execute();
        $result = $stmt->fetch();
        if ($result > 0) {

            return false;

        }else{
            $hashedPassword = hash('sha256', $password);
            $sql = "INSERT INTO customers(email,password,fname, phone, country_id) VALUES (:email,:password,:fname,:phone, :country_id);";
            $stmt = $db->prepare($sql);
            $stmt->bindValue(":email", $email);
            $stmt->bindValue(":password", $hashedPassword);
            $stmt->bindValue(":fname", $name);
            $stmt->bindValue(":phone", $phone_number);
            $stmt->bindValue(":country_id", $country);
            $re = $stmt->execute();

            if ($re) {
            $this->authentication($email, $password);
                return true;
            } else {
                return false;
            }


        }
    }




    public function authentication($email, $password)
    {
        $db = $this->getConnection();

        $hashedPassword = hash('sha256', $password);

        $result = $db->prepare("SELECT * FROM customers WHERE email=:email AND password=:password ");
        $result->execute([
            "email" => $email,
            "password" => $hashedPassword
        ]);

        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {


                $_SESSION['user_id'] = $rows["id"];
                $_SESSION['fname'] = $rows["fname"];
                $_SESSION['lname'] = $rows["lname"];
                $_SESSION['email'] = $rows["email"];


                echo "<script language = javascript>
               swal('Correct', 'you are successfully logged on', 'success');
            </script>";

               // header("Refresh:1");
                header('Refresh: 1; Confirmation.php');


            }
        } else {
            echo "<script language = javascript>
              swal('Oops...','Incorrect password or email!','error');
            </script>";
            header("Refresh:1");
        }
    }




    public function returnIdOfSendingCountry()
    {
        $db = $this->getConnection();


        $result = $db->prepare("SELECT * FROM country WHERE currency_code=:currency_code");
        $result->execute([
            "currency_code" => $_SESSION['from_currency']
        ]);

        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {


                return $rows["id"];

            }
        }


    }


        public function returnIdOfRecievingCountry()
        {
            $db = $this->getConnection();


            $result = $db->prepare("SELECT * FROM country WHERE currency_code=:currency_code");
            $result->execute([
                "currency_code" => $_SESSION['to_currency']
            ]);

            if ($result->rowCount()) {

                while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {


                    return $rows["id"];

                }
            }


        }




    public function returnBankById($id)
    {
        $db = $this->getConnection();


        $result = $db->prepare("SELECT * FROM banks WHERE id=:id");
        $result->execute([
            "id" => $id
        ]);

        if ($result->rowCount()) {

            while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {


                return $rows["id"];

            }
        }


    }






    // select all State
    public function returnSendCountry(){
        $db = $this->getConnection();

        try {
            $sql = "SELECT * FROM country WHERE isEnabled=:bool AND currency_code!=:cc";
            $stmt = $db->prepare($sql);
            $stmt->bindValue(":bool",1);
            $stmt->bindValue(":cc","NGN");

            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result > 0) {

             // print_r(json_encode($result)); exit();

               // $obj = json_encode($result);

               return $result;


               // return $result;
            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }

    }









    // select all State
    public function returnRecievingCountry(){
        $db = $this->getConnection();

        try {
            $sql = "SELECT * FROM country WHERE isEnabled=:bool AND currency_code!=:cc";
            $stmt = $db->prepare($sql);
            $stmt->bindValue(":bool",1);
            $stmt->bindValue(":cc","NGN");


            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result > 0) {


                return $result;


                // return $result;
            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }

    }



    public function returnAllCountries(){
        $db = $this->getConnection();

        try {
            $sql = "SELECT * FROM country WHERE isEnabled=:bool";
            $stmt = $db->prepare($sql);
            $stmt->bindValue(":bool",1);

            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result > 0) {

                // print_r(json_encode($result)); exit();

                // $obj = json_encode($result);

                return $result;


                // return $result;
            } else {
                return false;
            }
        }catch (PDOException $ex){
            return false;
        }

    }



    public function returnAvailableBanksInBeneficiaryCountry($to_currency){


            $db = $this->getConnection();

//return "currency code".$to_currency; exit();

            $result = $db->prepare("SELECT * FROM country WHERE currency_code=:currency_code");
            $result->execute([
                "currency_code" => $to_currency
            ]);

            if ($result->rowCount()) {

                while ($rows = $result->fetch(PDO::FETCH_ASSOC)) {


                    $beneficiary_country_id = $rows["id"];


                    try {
                        $sql = "SELECT * FROM banks WHERE country_id=:country_id";
                        $stmt = $db->prepare($sql);
                        $stmt->bindValue(":country_id", $beneficiary_country_id);

                        $stmt->execute();
                        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        if ($result > 0) {

                            return $result;
                        } else {
                            return false;
                        }
                    } catch (PDOException $ex) {
                        return false;
                    }


                }
            }
        }


}


?>