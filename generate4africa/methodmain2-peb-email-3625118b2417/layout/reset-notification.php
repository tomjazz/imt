
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">  
  <div marginwidth="0" marginheight="0" style="margin:0;padding:25px 0;min-height:100%!important;width:100%!important">
    <center>
      <table align="left" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;margin:0;padding:0;background-color:#ffffff;height:100%!important;max-width:600px!important">
        <tbody><tr>
          <td align="center" valign="top" style="margin:0;padding:0;border-top:0;height:100%!important;width:100%!important">

            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
              <tbody>
              <tr>
                <td align="center" valign="top">

                  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#ffffff;border-top:0;border-bottom:0">
                    <tbody><tr>
                      <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                          <tbody><tr>
                            <td valign="top" style="padding-top:10px;padding-bottom:10px"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                              <tbody>
                                <tr>
                                  <td valign="top">

                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                                      <tbody>
                                        <tr>
                                        <td valign="top" style="padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align:left;">

                                          <a href="http://payelectricitybills.com" title="Skrife.com" style="word-wrap:break-word" target="_blank">
                                            <img style="margin-top:20px;margin-bottom:5px;" align="center" alt="" src="http://payelectricitybills.com/img/peb-logo.png" width="125">
                                          </a>

                                        </td>
                                      </tr>
                                      <tr>
                                        <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;color:#333333;font-family:Helvetica;font-size:15px;line-height:150%;text-align:left">
                                          <tr>
                                          <td style="width:50%;padding-top:9px;padding-right:18px;padding-bottom:9px;padding-left:18px;">
                                          <h2 style="text-align:left;margin:0;padding:0;display:block;font-family:'Open Sans',Helvetica,Arial;font-size:20px;font-style:normal;font-weight:bold;line-height:125%;letter-spacing:-.75px;color:#0E436E!important">
                                            <span style="line-height:30px">Reset Password</span>
                                          </h2>
                                          </td>
                                          <td style="text-align:right;width:50%;font-family:'Open Sans',Helvetica,Arial">
                                        
                                          </td>
                                          </tr>
                                          <tr>
                                          <td colspan="2"  style="width:30%;font-size:14px;padding:25px 10px;font-family:'Open Sans',Helvetica,Arial;padding-left:18px;">
                                            <div style="text-align:left"><p><?php echo $notify['name']; ?>, </p>
                                            <p>You recently requested a password reset on your <strong>PayElectricityBills.com</strong> account. Please click the link below or copy it into your browser to complete the process.</p>
                                            <p><a href="<?php echo $notify['url'];?>"><?php echo $notify['url'];?></a></p>
                                            <p> If you did not initiate this process, feel free to ignore this message.</p>
                                                                                  </td>
                                          </tr>
                                        </tbody>
                                        </table>

                                      </td>
                                    </tr>
                                    <tr>

                                        <td valign="top" style="padding-top:9px;padding-right:18px;padding-bottom:50px;padding-left:18px;color:#333333;font-family:'Open Sans',Helvetica,Arial;font-size:15px;line-height:150%;text-align:left">
                                          <div style="text-align:left"><span style="color:#d3d3d3"><span style="font-family:'Open Sans',Helvetica,Arial;font-size:small;line-height:18.2000007629395px">&mdash;</span></span><br>
                                            <span style="font-size:14px"><span style="line-height:20.7999992370605px;text-align:center">Sincerely, <br/><strong>PEB Support team</strong></span><br style="line-height:20.7999992370605px;text-align:center">
                                            <a href="mailto:support@payelectricitybills.com" style="line-height:20.7999992370605px;text-align:center;word-wrap:break-word;color:#F5A623;font-weight:normal;text-decoration:underline" target="_blank"><span style="color:#F5A623">support@payelectricitybills.com</span></a></span></div>
                                            <div style="text-align:left;font-size:12px;margin-top:20px;color:#333;">Copyright &copy; 2016 Pay Electricity Bills, All rights reserved.</div>
                                          </td>
                                        </tr>
                                  </tbody>
                                </table>
                                </td>
                            </tr>
                          </tbody></table>
                        </td>
                      </tr>
                    </tbody>
                  </table>

                  </td>
                </tr>
              </tbody></table>

            </td>
          </tr>
        </tbody></table>
      </center>
    </div>

